\documentclass[border=5mm]{article}
\usepackage{tabularx} % extra features for tabular environment
\usepackage{amsmath}  % improve math presentation
\usepackage{graphicx} % takes care of graphic including machinery
\usepackage[margin=1in,letterpaper]{geometry} % decreases margins
\usepackage{cite} % takes care of citations
\usepackage[final]{hyperref} % adds hyper links inside the generated pdf file
\usepackage{blindtext}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage[most]{tcolorbox}
\usepackage{xparse}
\usepackage{mathtools}
\usepackage{lipsum}
\usepackage{tikz}
\usepackage{forest}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{amssymb}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{fancybox}
\usepackage{subcaption}
\usepackage{pgffor}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{amssymb}
\usepackage [autostyle, english = american]{csquotes}
\usepackage [english]{babel}

\usetikzlibrary{
    calc,
	arrows.meta,
	fit,
	backgrounds,
	positioning,
	chains,
  	shapes.multipart,
  	arrows.meta 
}

\hypersetup{
	colorlinks=true,       % false: boxed links; true: colored links
	linkcolor=blue,        % color of internal links
	citecolor=blue,        % color of links to bibliography
	filecolor=magenta,     % color of file links
	urlcolor=blue
}

\newcommand{\sep}{\hspace*{.5em}}
\usetikzlibrary{calc}
\newcounter{nodeidx}
\newcounter{example}
\setcounter{nodeidx}{1}

\def\exampletext{Example} % If English

\NewDocumentEnvironment{testexample}{ O{} }
{
\colorlet{colexam}{red!55!black} % Global example color
\newtcolorbox[use counter=example]{testexamplebox}{%
    % Example Frame Start
    empty,% Empty previously set parameters
    title={\exampletext {} \thetcbcounter: #1},% use \thetcbcounter to access the testexample counter text
    % Attaching a box requires an overlay
    attach boxed title to top left,
       % Ensures proper line breaking in longer titles
       minipage boxed title,
    % (boxed title style requires an overlay)
    boxed title style={empty,size=minimal,toprule=0pt,top=4pt,left=3mm,overlay={}},
    coltitle=colexam,fonttitle=\bfseries,
    before=\par\medskip\noindent,parbox=false,boxsep=0pt,left=3mm,right=0mm,top=2pt,breakable,pad at break=0mm,
       before upper=\csname @totalleftmargin\endcsname0pt, % Use instead of parbox=true. This ensures parskip is inherited by box.
    % Handles box when it exists on one page only
    overlay unbroken={\draw[colexam,line width=.5pt] ([xshift=-0pt]title.north west) -- ([xshift=-0pt]frame.south west); },
    % Handles multipage box: first page
    overlay first={\draw[colexam,line width=.5pt] ([xshift=-0pt]title.north west) -- ([xshift=-0pt]frame.south west); },
    % Handles multipage box: middle page
    overlay middle={\draw[colexam,line width=.5pt] ([xshift=-0pt]frame.north west) -- ([xshift=-0pt]frame.south west); },
    % Handles multipage box: last page
    overlay last={\draw[colexam,line width=.5pt] ([xshift=-0pt]frame.north west) -- ([xshift=-0pt]frame.south west); },%
    }
\begin{testexamplebox}}
{\end{testexamplebox}\endlist}

\renewcommand{\lstlistingname}{Code}% Listing -> Code
\renewcommand{\lstlistlistingname}{List of \lstlistingname s}

\newcommand{\nodes}[1]{%
    \foreach \num [count=\n starting from 0] in {#1}{% no need for an external counter
      \node[minimum size=6mm, draw, rectangle] (n\n) at (\n,0) {\num};
    }
}

\newcommand{\brckt}[4][1]{% [ lvl ] { from, to, text }
    \coordinate (left) at ($(n#2.south west)+(-2mm,-1mm)$);
    \coordinate (right) at ($(n#3.south east)+(2mm,-1mm)$);
    \draw (left) -- ($(left)+(0,-1mm*#1)$) --node[below,midway,font=\scriptsize] {#4} ($(right)+(0,-1mm*#1)$) -- (right);
}

%% code section setting definition
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=C++,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\forestset{%
  label tree/.style={
    for tree={tier/.option=level},
    level label/.style={
      before typesetting nodes={
        for nodewalk={current,tempcounta/.option=level,group={root,tree breadth-first},ancestors}{if={>OR={level}{tempcounta}}{before drawing tree={label me=##1}}{}},
      }
    },
    before drawing tree={
      tikz+={\coordinate (a) at (current bounding box.north);},
    },
  },
  label me/.style={tikz+={\node [anchor=base west] at (.parent |- a) {#1};}},
}

\begin{document}

\title{SpyGoIPFS: A lite client for IPFS}
\author{Vincenzo Palazzo \\v.palazzo1@studenti.unipi.it}
\date{\today}
\maketitle

\begin{abstract}
	SpyGoIPFS is a console application written in Go language that implements a lite client to download content from IPFS network, 
	and also it contains the logic to implement analysis over the network. It is developed with the go modules offers from Protocols Lab \cite{ipfs:author} 
	that is the main team that develops and maintains the IPFS ecosystem.
\end{abstract}

\section{Introduction}

The paper is divided into two main sections where the Section \ref{sec:first_sec} describes the practical part with the details on the go application to download the files
and make the analysis of the network during the download. Section \ref{sec:second_sec} describes the theoretical part of the paper with the answer to the question
proposed for the midterm.\\
In the Section \ref{sec:related_work} there are the conclusion with the focus on how to run the project with docker compose to avoid problem with different system and 
different version of languages.
	
\section{First Part}
\label{sec:first_sec}

The midterm gives the possibility to choose between the different approach to develop the application to make the download operation over the network
and after a small analysis of the different API given by the IPFS ecosystem, the choice is to use the go modules that are used to implement the go-ipfs 
node. In addition, the Go API is the only library that has full support for the bitswap modules.
The last motivation but not least, with these go modules are possible to develop a complete lite node without configuring an ipfs node in the local machine,
 but the SpyGoIPFS application is enough to download the content from the network.

\subsection{How It Works}

SpyGoIPFS can be compiled with the \emph{go build .} command or with the command \emph{make}  and it produces an executable for 
the user machine compatible with the architecture called \emph{SpyGoIPFS}, and it has some rules that are summarized in the next sections.\\
After the first run, the application creates a local directory called \emph{.spygo} that contains all the data produced from the application, 
and this directory is create in the same position as the runnable program.\\
SpyGoIPFS, implement a complete lite node in \emph{only receive} mode, and the configuration on the distributed hash table is reported in the
code \ref{code:lib2p2_routingtb}. With the following code is possible to use the node only to receive the information.
 
\begin{figure}[htpb]
	\lstinputlisting[language=GO, 
		label={code:lib2p2_routingtb},
		caption={SpyGoIPFS configuration routing table.},
		captionpos=b]
		{code/lib2p2_routingtb.go}
\end{figure}

\subsection{Download}

SpyGoIPFS give the possibility to download one content per time in the directory \emph{.spygo}, and to download the file in the correct form
it uses the ipfs go module called go-ipfs-files \cite{ipfs:file}.
During the download the application will print some information on the console about the status of the download, and at the end of it; the program
will end. The log of the application is managed by the environment, to enable the debug mode is possible to export the propriety LOG="debug".

An example of command is reported in the Figure \ref{fig:down_ex}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{imgs/down-ex.png}
  \caption{Screenshot to show how the program looks like during the download.}
  \label{fig:down_ex}
\end{figure}

And the command is reported in the Code \ref{code:download_command}

\begin{figure}[htpb]
	\lstinputlisting[language=Bash, 
		label={code:download_command}, 
		caption={Example of a command to run the download of content over the IPFS network.},
		captionpos=b]
		{code/download_command.bash}
\end{figure}

\subsection{Analysis}

To start the analysis of the network during the download of a file the user needs to specify the option {\bf -a}, and the software will produce
three different json in the .spygo called \emph{info.json}, \emph{lookup.json} and \emph{contributors.json}.

Where these files contain the following information

\begin{itemize}
  \item \emph{info.json}: It contains the information's relative to the node and the download of the file;
  \item \emph{lookup.json}: It contains the information's relative to the peers, it includes all info relative to the peers, included geolocalization;
  \item \emph{contributors.json}: It contains all information on how much the peers has contributed to the download of the file.
\end{itemize}

Each time that a new analysis is run these files are overwritten. In addition, the analysis described in the following sections are made with a clean instance
of the node each time.

The \emph{contributors.json} looks as in the Code \ref{code:contributors_res} and the Figure \ref{fig:down_ex} shows a plot in a Pie Chart 
of the data.

\begin{figure}[H]
	\lstinputlisting[language=Bash, 
		label={code:contributors_res}, 
		caption={Example of a command to run the download of a content over the IPFS network.},
		captionpos=b]
		{code/contributors.json}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.3]{imgs/contributors.png}
  \caption{Screenshot to show an how the program looks like during the download.}
  \label{fig:down_ex}
\end{figure}

The \emph{lookup.json} looks as in the Code \ref{code:contributors_res} and the Figure \ref{fig:lookup} shows a plot in a map chart of the data.

\begin{figure}[H]
	\lstinputlisting[language=Bash, 
		label={code:lookup_res}, 
		caption={Example of a command to run the download of a content over the IPFS network.},
		captionpos=b]
		{code/lookup.json}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.3]{imgs/lookup.png}
  \caption{Screenshot to show an how the program looks like during the download.}
  \label{fig:lookup}
\end{figure}

\subsection{Example of Analysis}

In this section is describe the analysis of the content share in the midterm assignment and in some additional content taken from
the awesome ipfs repository, in particular on the following content

\begin{itemize}
  \item XKCD Comics with CID QmdmQXB2mzChmMeKY47C43LxUdg1NDJ5MWcKMKxDu7RgQm, with a dimension around 107 MB;
  \item Old Internet files with CID QmbsZEvJE8EU51HCUHQg2aem9JNFmFHdva3tGVYutdCXHp, with a dimension around 210 MB;
  \item Apollo Archives with CID QmSnuWmxptJZdLJpKRarxBMS2Ju2oANVrgbr2xWbie9b2D, with a dimension around 59 GB.
  \item The site \enquote{textfiles.com} CID QmNoscE3kNc83dM5rZNUC5UDXChiTdDcgf16RVtFCRWYuU, with a dimension around 1.6 GB
\end{itemize}

the analysis on the \enquote{Old Internet files} is pretty simple because the file is only 210 MB and the peers that contribute to the download are not a lot,
In particular, after a couple of tests, the result shows that the content is shared from the same country in Germany, and the analysis
of the network has the result shows in the Figure \ref{fig:small_res}

\begin{figure}[H]
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{imgs/small_ex.png}
    \caption{Data Received from the peers.}
    \label{fig:sfig1}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{imgs/small_ex_loc.png}
    \caption{Location of the peers in the world.}
    \label{fig:sfig2}
  \end{subfigure}
  \caption{Result of the analysis on the Old Internet files content.}
  \label{fig:small_res}
\end{figure}

All the information about the peers are summarized in the Table \ref{tab:01}.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.34]{imgs/table02.png}
  \caption{Summary of the peer information's.}
  \label{tab:01}
\end{figure}

With the result of the analysis show in Figure \ref{fig:small_res}, the next analysis is made on the content \enquote{XKCD Comics}
and the analysis shows an interesting result that is shows in Figure \ref{fig:smaller_res}

\begin{figure}[H]
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{imgs/smaller_data.png}
    \caption{Data Received from the peers.}
    \label{fig:sfig1}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{imgs/smaller_loc.png}
    \caption{Location of the peers in the world.}
    \label{fig:sfig2}
  \end{subfigure}
  \caption{Result of the analysis on the XKCD Comics content.}
  \label{fig:smaller_res}
\end{figure}

The interesting information that shows the Figure \ref{fig:smaller_res} is that also if the file that the node is asking is smaller, there are 
a lot of nodes that will contribute to download it.

All the information about the peers are summarized in the Table \ref{tab:02}. The information's contains the node information such as port and protocol
used to each node.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.35]{imgs/table01.png}
  \caption{Summary of the peer information's.}
  \label{tab:02}
\end{figure}

Another analysis is made with a bigger file take into consideration in this paper, that is the content of \enquote{Apollo Archives}. 
The Figure \ref{fig:bigger_res} show the result.

\begin{figure}[H]
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{imgs/bigger_data.png}
    \caption{Data Received from the peers.}
    \label{fig:sfig1}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{imgs/bigger_loc.png}
    \caption{Location of the peers in the world.}
    \label{fig:sfig2}
  \end{subfigure}
  \caption{Result of the analysis on the Apollo Archives.}
  \label{fig:bigger_res}
\end{figure}

All the information about the peers are summarized in the Table \ref{tab:03}.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.35]{imgs/table03.png}
  \caption{Summary of the peer information's.}
  \label{tab:03}
\end{figure}

The last analysis is made on the content of the site \enquote{textfiles.com} with a dimension around 1.9 GB and the result of the analysis
is shows in the Figure \ref{fig:medium_res}

\begin{figure}[H]
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{imgs/medium_data.png}
    \caption{Data Received from the peers.}
    \label{fig:sfig1}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{imgs/medium_loc.png}
    \caption{Location of the peers in the world.}
    \label{fig:sfig2}
  \end{subfigure}
  \caption{Result of the analysis on the \enquote{textfiles.com} site.}
  \label{fig:medium_res}
\end{figure}

All the information about the peers are summarized in the Table \ref{tab:04}.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.35]{imgs/table04.png}
  \caption{Summary of the peer information's.}
  \label{tab:04}
\end{figure}

A complete description of the analysis with the Jupiter notebook is available on Gitlab \cite{vincenzopalazzo:jupiter}.

\section{Second Part}
\label{sec:second_sec}
\subsection{Kademlia Protocol vs IPFS Kademlia Protocol (S/Kademlia)}

IPFS protocol uses a modified version of Kademlia protocol, in particular, IPFS uses a version of Kademlia
called S/Kademlia \cite{10.1007/3-540-45748-8_5} that contains some security improvements. One of the big 
improvements introduce is the limitation in the content identifier. In fact, the S/Kademlia use the \emph{Proof Of Work} to verify a new node.
In addition, the implementation includes the parallel looks ups (Originally supported also in Kademlia) over a multiple
disjoin path, and the Kademlia table contains also the list of siblings.

\subsection{Kademlia leaks}

As described in the previous section the Kademlia as describe in the paper \cite{10.1007/3-540-45748-8_5} contains some leaks that the S/Kademlia resolve,
one of these leak are described briefly below:

\begin{itemize}
  \item Eclipse attack: This attack tries to place adversarial nodes in the network in a way that one or more nodes are cut off from it;
  \item Sybil attack: In completely decentralized systems (as IPFS) there is no instance that controls the quantity of node Ids an attacker 
        can obtain;
  \item Adversarial routing: Since a node is simply removed from a routing table when it neither responds with routing information nor routes 
        any packet, the only way of influencing the networks routing is to return adversarial routing information.
\end{itemize}

A interesting paper where these information are taken is \enquote{S/Kademlia: A Practicable Approach Towards Secure} \cite{4447808} 

\subsection{Kademlia Distance Metrics vs Chord Distance Metrics}

The key difference between Chord and Kademlia is the definition of \emph{closeness} between points in the identifier space. 
With the distance function is possible define the meaning of closeness between two points.

\[dinstance \mapsto d(pointA, pointB)\]

\subsubsection{Chord: Distance function}

The distance function used in Chord can be expressed as  $d(x,y) \mapsto (y - x) \mod 2^m$ where m is the number of the bits.
As described in the Chord paper \cite{10.1145/383059.383071}, this can be visualized by putting all m-bit on a circle as shows 
in the Figure \ref{fig:chord_image}.\\
The distance function $d(pointA, pointB)$ on the Figure \ref{fig:chord_image} mean the numbers of steps (jumps) to to reach the $pointB$ 
from $pointA$ in clock-wise direction.

%A key with image $k$ is then mapped to the node whose image $n$ minimizes the value of d(k,n) .

\begin{figure}[H]
  \centering
  \includegraphics[width=.4\linewidth]{imgs/Chord-lookup-procedure.png}
  \caption{Distance representation between peers with Chord DHT.}
  \label{fig:chord_image}
\end{figure}

\subsubsection{Kademlia: Distance function}

The distance function used in Kademlia is  $d(x,y) \mapsto x \oplus y$ where the $\oplus$ indicate the bit-wise XOR operator.
As described in the Kademlia paper \cite{10.1145/383059.383071} the distance between peers can be visualized with the help of a 
binary tree as the Figure \ref{fig:kademlia_image} shows.\\ d(x,y)  is then the height of the sub-tree rooted at the lowest-common ancestor 
of the corresponding nodes in the binary tree.


\begin{figure}[H]
  \centering
  \includegraphics[width=.6\linewidth]{imgs/kademlia.png}
  \caption{Distance representation between peers with Kademlia DHT.}
  \label{fig:kademlia_image}
\end{figure}


\section{Conclusion}
\label{sec:related_work}

All the code solution developed for this assignment are release on Gitlab with a complete docker support. In particular,
the Go project contains a docker image and a docker-compose that give the possibility to run the docker image with the
host volume shared with one simple command.

To run the docker compose is possible run the following commands.

\begin{figure}[H]
	\lstinputlisting[language=Bash, 
		label={code:lookup_res}, 
		caption={Example of an command to run the project with docker compose.},
		captionpos=b]
		{code/spygoipfs_docker.bash}
\end{figure}

To build the SpyGoIPFS on the host system without using docker it is possible run the commands in the Figure \ref{code:from_source}

\begin{figure}[H]
	\lstinputlisting[ 
		label={code:from_source}, 
		caption={Build SpyGoIPFS from source.},
		captionpos=b]
		{code/from_source.bash}
\end{figure}

In conclusion, the report with the Jupiter notebook contains also a docker compose that give the possibility to avoid to install
dependencies on the system and it is possible run the docker image with the commands in the Figure \ref{code:jupiter}.\\
The Jupiter notebook is available on the link printed to the console after the end of the build.

\begin{figure}[H]
	\lstinputlisting[language=Bash, 
		label={code:jupiter}, 
		caption={Example of an command to run the Jupiter notebook with docker compose.},
		captionpos=b]
		{code/jupiter_docker.bash}
\end{figure}

\bibliography{ref} 
\bibliographystyle{ieeetr}

\end{document}