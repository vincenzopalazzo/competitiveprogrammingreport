#!/bin/bash
docker-compose up

## After building
[I 13:00:54.043 NotebookApp] [jupyter_nbextensions_configurator] enabled 0.4.1
[I 13:00:54.044 NotebookApp] Serving notebooks from local directory: /home/vincent/GithubUnipi/SpyGoIpfs/report
[I 13:00:54.044 NotebookApp] Jupyter Notebook 6.2.0 is running at:
[I 13:00:54.044 NotebookApp] http://localhost:8888/?token=0adfaae5e78e9162e8e61fcbc8d16d6decb3636083c07f25
[I 13:00:54.044 NotebookApp]  or http://127.0.0.1:8888/?token=0adfaae5e78e9162e8e61fcbc8d16d6decb3636083c07f25
[I 13:00:54.044 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 13:00:54.087 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///home/vincent/.local/share/jupyter/runtime/nbserver-1268344-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/?token=0adfaae5e78e9162e8e61fcbc8d16d6decb3636083c07f25
     or http://127.0.0.1:8888/?token=0adfaae5e78e9162e8e61fcbc8d16d6decb3636083c07f25