#!/bin/bash
docker-compose up
docker-compose run -T spygoipfs bash
>> ./SpyGoIPFS --help
usage: SpyGoIpfs [-h|--help] [-v|--version] [-d|--download "<value>"]
                 [-a|--analysis] [-p|--peer "<value>"]

                 SpyGoIpfs is an academic application to work with a IPFS.

Arguments:

  -h  --help      Print help information
  -v  --version   Print version application. Default: false
  -d  --download  Download all the reference from the IPFS url
  -a  --analysis  Make available on the analysis about the download. Default:
                  false
  -p  --peer      Connect to a specific peers (particular util for the first
                  used). Default: 