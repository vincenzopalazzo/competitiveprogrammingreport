nodeOptions := &core.BuildCfg{
	Online: true,
	//Routing: libp2p.DHTOption, // This option sets the node to be a full DHT node (both fetching and storing DHT Records)
	Routing: libp2p.DHTClientOption, // This option sets the node to be a client DHT node (only fetching records)
	Repo:    localDir,
}
