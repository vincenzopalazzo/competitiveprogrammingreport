\beamer@sectionintoc {1}{Recall Procedure Abstraction}{3}{0}{1}
\beamer@sectionintoc {2}{Recall Object-oriented languages}{15}{0}{2}
\beamer@sectionintoc {3}{Problems}{21}{0}{3}
\beamer@sectionintoc {4}{Scope Visibility}{32}{0}{4}
\beamer@sectionintoc {5}{Methods Dispatch}{56}{0}{5}
\beamer@sectionintoc {6}{Multiple inheritance}{77}{0}{6}
\beamer@sectionintoc {7}{Recalls}{83}{0}{7}
