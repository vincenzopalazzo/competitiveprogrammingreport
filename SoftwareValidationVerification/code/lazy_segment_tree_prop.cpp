void propagate(std::size_t start_index, std::size_t left_index,
               std::size_t right_index) {
  if (lazy[start_index] != -1) {
    // The node in position start_index was marked as lazy
    // during the update phases
    structure[start_index] += lazy[start_index];
    if (left_index != right_index) {
      auto left_child = left_child_index(start_index);
      auto right_child = right_child_index(start_index);
      lazy[left_child] = lazy[right_child] = lazy[start_index];
    } else {
      // left_index = right_index is the time to update the origin array
      origin[left_index] = lazy[start_index];
    }
    // mark as the node as not lazy
    // we propagate the change to the leafs
    lazy[start_index] = -1;
  }
}