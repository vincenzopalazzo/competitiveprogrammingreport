template <typename T>
static std::vector<T> remapping_original_array(
    std::vector<std::pair<T, std::size_t>> &inputs) {
  std::sort(inputs.begin(), inputs.end());
  auto index = 1;
  std::vector<T> remapping(inputs.size());
  for (auto elem : inputs) {
    remapping[elem.second] = index;
    index++;
  }
  return remapping;
}