\documentclass[border=5mm]{article}
\usepackage{tabularx} % extra features for tabular environment
\usepackage{amsmath}  % improve math presentation
\usepackage{graphicx} % takes care of graphic including machinery
\usepackage[margin=1in,letterpaper]{geometry} % decreases margins
\usepackage{cite} % takes care of citations
\usepackage[final]{hyperref} % adds hyper links inside the generated pdf file
\usepackage{blindtext}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage[most]{tcolorbox}
\usepackage{xparse}
\usepackage{lipsum}
\usepackage{tikz}
\usepackage{forest}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{amssymb}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{fancybox}
\usepackage{pgffor}
\usepackage{listings}
\usepackage{booktabs}
\usepackage [autostyle, english = american]{csquotes}
\usepackage [english]{babel}

\usetikzlibrary{
    calc,
	arrows.meta,
	fit,
	backgrounds,
	positioning,
	chains,
  	shapes.multipart,
  	arrows.meta 
}

\hypersetup{
	colorlinks=true,       % false: boxed links; true: colored links
	linkcolor=blue,        % color of internal links
	citecolor=blue,        % color of links to bibliography
	filecolor=magenta,     % color of file links
	urlcolor=blue
}

\newcommand{\sep}{\hspace*{.5em}}
\usetikzlibrary{calc}
\newcounter{nodeidx}
\newcounter{example}
\setcounter{nodeidx}{1}

\renewcommand{\lstlistingname}{Code}% Listing -> Code
\renewcommand{\lstlistlistingname}{List of \lstlistingname s}
\newcommand{\chapquote}[3]{\begin{quotation} \textit{#1} \end{quotation} \begin{flushright} - #2, \textit{#3}\end{flushright} }

\newcommand{\nodes}[1]{%
    \foreach \num [count=\n starting from 0] in {#1}{% no need for an external counter
      \node[minimum size=6mm, draw, rectangle] (n\n) at (\n,0) {\num};
    }
}

\newcommand{\brckt}[4][1]{% [ lvl ] { from, to, text }
    \coordinate (left) at ($(n#2.south west)+(-2mm,-1mm)$);
    \coordinate (right) at ($(n#3.south east)+(2mm,-1mm)$);
    \draw (left) -- ($(left)+(0,-1mm*#1)$) --node[below,midway,font=\scriptsize] {#4} ($(right)+(0,-1mm*#1)$) -- (right);
}

%% code section setting definition
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=C++,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\forestset{%
  label tree/.style={
    for tree={tier/.option=level},
    level label/.style={
      before typesetting nodes={
        for nodewalk={current,tempcounta/.option=level,group={root,tree breadth-first},ancestors}{if={>OR={level}{tempcounta}}{before drawing tree={label me=##1}}{}},
      }
    },
    before drawing tree={
      tikz+={\coordinate (a) at (current bounding box.north);},
    },
  },
  label me/.style={tikz+={\node [anchor=base west] at (.parent |- a) {#1};}},
}

\begin{document}

\title{Software Validation and Verification}
\author{Vincenzo Palazzo \\v.palazzo1@studenti.unipi.it}
\date{\today}
\maketitle
	
\section{Introduction}

Put some information here!

\section{Lessons 1}
\chapquote{``Introduction to model checking and to control-intensive transition systems"}{Tue}{14/09}

It is all about money. We are annoyed when our mobile phone malfunctions, or when
our video recorder reacts unexpectedly and wrongly to our issued commands. These
software and hardware errors do not threaten our lives, but may have substantial financial
consequences for the manufacturer. Correct ICT systems are essential for the survival of
a company. Dramatic examples are known. The bug in Intel’s Pentium II floating-point
division unit in the early nineties caused a loss of about 475 million US dollars to replace
faulty processors, and severely damaged Intel’s reputation as a reliable chip manufacturer.
The software error in a baggage handling system postponed the opening of Denver’s airport
for 9 months, at a loss of 1.1 million US dollar per day.

\subsubsection{What is System Verification?}

System verification amounts to check whether a system fulfills
the qualitative requirements that have been identified. However, 
verification is different from validation for the following reason:
\begin{itemize}
  \item {\bf Verification}: Check that we are building the thing {\bf right};
  \item {\bf Validation}: Check that we are building the {\bf right} thing.
\end{itemize}

\subsubsection{Software Verification Techniques}

A briflly recall to Software Verification Techniques, that we don't need to recall (I think).

\begin{itemize}
  \item {\bf Peer reviewing}: static technique: manual code inspection, no software execution. 
        However, subtle errors (concurrency and algorithm defects) hard to catch;
  \item {\bf Testing}: dynamic technique in which software is executed;
\end{itemize}

30\% to 50\% of software project costs devoted to testing, and more time is spent on validation than on construction

\subsubsection{Formal Methods}
\chapquote{``Formal methods should be part of the education of every computer scientist
and software engineer, just as the appropriate branch of applied maths is a
necessary part of the education of all other engineers."}{Tue}{14/09}

Formal methods are the {\bf applied mathematics for modelling and analysing ICT systems}, 
and Formal methods offer a large potential for:

\begin{itemize}
  \item Obtaining an early integration of verification in the design process;
  \item Providing more elective verification techniques (higher coverage);
  \item Reducing the verification time.
\end{itemize}

To increase the importance of the Formal Methods, and make the softwer verification with formal method there
is a raccomandation to use it by IEC, FAA, and NASA for safety-critical software.

\subsubsection{Formal Methods Techinique}

In software and hardware design of complex systems, more time and effort are spent on
verification than on construction. Techniques are sought to reduce and ease the verification
efforts while increasing their coverage. Formal methods offer a large potential to obtain an
early integration of verification in the design process, to provide more effective verification
techniques, and to reduce the verification time.
Let us first briefly discuss the role of formal methods. To put it in a nutshell, formal
methods can be considered as “the applied mathematics for modeling and analyzing ICT
systems”. Their aim is to establish system correctness with mathematical rigor. Their
great potential has led to an increasing use by engineers of formal methods for the ver-
ification of complex software and hardware systems.

\subsubsection{Deductive Method}

Provide a formal proof that P holds, to implement this we have some tools like theorem prover/proof assistant or proof checker).
This techinique system has form of a mathematical theory.

\subsubsection{Model Checking}

Model checking is a verification technique that explores all possible system states in a
brute-force manner. Similar to a computer chess program that checks possible moves, a
model checker, the software tool that performs the model checking, examines all possible
system scenarios in a systematic manner. In this way, it can be shown that a given system
model truly satisfies a certain property. It is a real challenge to examine the largest possible
state spaces that can be treated with current means, i.e., processors and memories.

One of the most famus system to implement model checking is SPIN, but this techinque it is applicable only if the system generates (finite) behavioural model.

\subsubsection{Model-based Simulation or Testing}

Provide a test for a propriety P by exploring possible behaviours. This tecnique it is applicable only if the system defines an executable model

\subsubsection{What is Model Checking?}

Model checking is an automated technique that, given a finite-state model of a system and a formal property,
systematically checks whether this property holds for (a given state in) that model.

In applying model checking to a design the following different phases can be distinguished:

\begin{itemize}
  \item {\bf Modeling phase}: 
  \begin{itemize}
    \item Model the system under consideration using the model description language of
    the model checker at hand; 
    \item as a first sanity check and quick assessment of the model perform some simu-
    lations; 
    \item formalize the property to be checked using the property specification language.
  \end{itemize}
  \item {\bf Running phase}: run the model checker to check the validity of the property in the
  system model;
  \item {\bf Analysis phase}:
  \begin{itemize}
    \item property satisfied? check next property (if any);
    \item property violated?
    \begin{itemize}
      \item analyze generated counterexample by simulation;
      \item refine the model, design, or property;
      \item repeat the entire procedure.
    \end{itemize}
    \item out of memory? try to reduce the model and try again.
  \end{itemize}
\end{itemize}

In addition to these steps, the entire verification should be planned, administered, and
organized. This is called verification organization.

\subsubsection{Pro and Cons of Model Checking}

\begin{itemize}
  \item {\bf Pro}:
  \begin{itemize}
    \item widely applicable (hardware, software, protocol systems, ...);
    \item allows for partial verification (only most relevant properties);
    \item potential “push-button” technology (software-tools);
    \item rapidly increasing industrial interest;
    \item in case of property violation, a counterexample is provided;
    \item sound and interesting mathematical foundations;
    \item not biased to the most possible scenarios (such as testing).
  \end{itemize}
  \item {\bf Cons}:
  \begin{itemize}
    \item main focus on control-intensive(See later) applications (less
    data-oriented);
    \item model checking is only as “good” as the system model;
    \item potential “push-button” technology (software-tools);
    \item model checking is only as “good” as the system model;
    \item impossible to check generalisations;
  \end{itemize}
\end{itemize}

\section{Lessons 2}
\chapquote{``Data-intensive transition systems and the modelling of parallel/communicating systems"}{Wed}{15/09}

\subsection{Modelling Concurrent Systems}

\bibliography{ref} 
\bibliographystyle{ieeetr}

\end{document}
