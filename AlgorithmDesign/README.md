# Advanced Algorithm 2020/2021 Exam

## Table of Content

- Exam Material
- License

## Exam Material

- [C++ Rolling Hasing implementation](https://github.com/vincenzopalazzo/cpstl/tree/master/cpp/algoritmins/karp_rabin_fingerprint);
- [String match with Rolling hash](https://github.com/vincenzopalazzo/cpstl/blob/master/python/tests/algorithm/hash/test_rolling_hash.py)
- [C++ Randomized Quick Sort](https://github.com/vincenzopalazzo/cpstl/tree/master/cpp/algoritmins/sort)
- [C++ Universal Hash Function](https://github.com/vincenzopalazzo/cpstl/tree/master/cpp/algoritmins/hash);
- [C++ Perfect hashing](https://github.com/vincenzopalazzo/cpstl/tree/master/cpp/algoritmins/hash);
- [C++ Cuckoo Hashing](https://github.com/vincenzopalazzo/cpstl/tree/master/cpp/algoritmins/hash);
- [C++ implementation Bloom Filter](https://github.com/vincenzopalazzo/cpstl/tree/master/cpp/data_structures/bloomfilter) with a [medium post description](https://vincenzopalazzo.medium.com/introduction-to-bloom-filter-by-examples-84691c2d859);
- [KNAPSACK implementation](https://github.com/vincenzopalazzo/benchmarks/tree/main/python/benchamarks/bm)
- [Flajolet–Martin (FM) Sketch algorithm](https://github.com/vincenzopalazzo/cpstl/blob/master/python/cpstl/cpstl/algorithm/sketch/flajolet_martin.py)
- [Count min sketch](https://github.com/vincenzopalazzo/cpstl/blob/master/python/cpstl/algorithm/sketch/count_min_sketch.py)
- [Min hash and Jaccard Similarity](https://github.com/vincenzopalazzo/cpstl/tree/master/python/cpstl/algorithm/similarity)

In particular a full python library was developer with most of the technique discussed during the
class https://github.com/vincenzopalazzo/cpstl

In addition, an deep drive in the Min Cost flow method is done with a report in the `report`
directory, where an probabilistic approach is discussed over a uncertain network.

## License

<div align="center">
  <img src="https://opensource.org/files/osi_keyhole_300X300_90ppi_0.png" width="150" height="150"/>
</div>

Advanced Algorithm exam materials.

Copyright (C) 2022 Vincenzo Palazzo <vincenzopalazzodev@gmail.com>.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
