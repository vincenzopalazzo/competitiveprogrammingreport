template <class T>
static bsp::BSP<T> setUpTikinAlgo(std::size_t item_size, int parallel_degree) {
  std::shared_ptr<bsp::Env<T>> env;
  if (parallel_degree > 0)
    env = std::make_shared<bsp::Env<T>>(parallel_degree);
  else
    env = std::make_shared<bsp::Env<T>>();
  bsp::BSP<T> bsp(env);

  std::random_device rd;
  std::mt19937_64 mt(rd());
  auto generator = std::uniform_int_distribution<int>(1, item_size);
  
  T input; // vector of type!
  std::generate(input.begin(), input.end(), [&]() { return generator(mt); });

  bsp::Msg<T> msg(0, input);
  env->add_message(0, msg);

  bsp::SuperStep<T> emitter;
  std::shared_ptr<bsp::tiskin::Emitter<T>> emitter_act =
      std::make_shared<bsp::tiskin::Emitter<T>>(0);
  emitter.add_activity(emitter_act);

  bsp.add_super_step(emitter);

  bsp::SuperStep<T> step_one;

  auto par_degree = item_size / env->core_to_use();
  for (std::size_t i = 1; i <= par_degree; i++) {
    auto step_one_act =
        std::make_shared<bsp::tiskin::StepOne<T>>(i, par_degree);
    step_one.add_activity(step_one_act);
  }

  bsp.add_super_step(step_one);

  bsp::SuperStep<T> step_two;

  for (std::size_t i = 1; i <= par_degree; i++) {
    auto step_two_act =
        std::make_shared<bsp::tiskin::StepTwo<T>>(i, par_degree);
    step_two.add_activity(step_two_act);
  }

  bsp.add_super_step(step_two);

  bsp::SuperStep<T> step_three;

  for (std::size_t i = 1; i <= par_degree; i++) {
    auto step_three_act = std::make_shared<bsp::tiskin::StepThree<T>>(i);
    step_two.add_activity(step_three_act);
  }

  bsp.add_super_step(step_three);

  return bsp;
}