R range_query_subroutine(std::size_t start_index, std::size_t left_index_now,
                         std::size_t right_index_now, T k) {
  if (structure[start_index] < k) return -1;
  // range represented by a node is completely inside
  // the given range
  if (left_index_now == right_index_now)
    return left_index_now;  
  // range represented by a node that is partially inside and partially outside the
  // given range
  auto middle_point = (left_index_now + right_index_now) / 2;
  auto left_child = left_child_index(start_index);
  auto right_child = right_child_index(start_index);
  if (structure[left_child] >= k)
    return range_query_subroutine(left_child, left_index_now, middle_point, k);
  return range_query_subroutine(right_child, middle_point + 1, right_index_now,
                                k - structure[left_child]);
}