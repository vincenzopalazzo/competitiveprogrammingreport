template <typename T>
std::vector<T> range_minimum_query_naive(std::vector<T> const &inputs,
                                         std::vector<Query<T>> const &queries) {
  std::vector<T> results;
  results.reserve(queries.size());
  for (auto &query : queries) {
    T minimum = INT32_MAX;
    for (std::size_t i = query.start; i <= query.end; i++) {
      if (inputs[i] < minimum) minimum = inputs[i];
    }
    results.push_back(minimum);
  }
  return results;
}