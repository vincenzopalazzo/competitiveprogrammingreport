int increment(int a) {
  a++;
  return a;
}

void increment_by_ref(int *a) { (*a)++; }

int main() {
  int a;
  a = 0;
  a = increment(a);
  print(a);
  increment_by_ref(&a);
  print(a);
  return 0;
}
