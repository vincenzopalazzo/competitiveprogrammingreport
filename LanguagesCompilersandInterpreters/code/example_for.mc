void insertionSort(int arr[], int n) {
  int i;
  int key;
  int j;

  for (i = 1; i < n; i = i + 1) {
    key = arr[i];
    j = i - 1;

    while (j >= 0 && arr[j] > key) {
      arr[j + 1] = arr[j];
      j = j - 1;
    }
    arr[j + 1] = key;
  }
}

void printArray(int arr[], int n) {
  for (int i = 0; i < n; i++) print(arr[i]);
}

int main() {
  int arr[5];

  arr[0] = 12;
  arr[1] = 11;
  arr[2] = 13;
  arr[3] = 5;
  arr[4] = 6;

  insertionSort(arr, 5);
  printArray(arr, 5);

  return 0;
}