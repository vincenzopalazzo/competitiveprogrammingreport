int max(int x, int y) {
  if (x < y) return y;
  return x;
}

int main() {
  int a = max(1, 2);
  print(a);
  return 0;
}