>> docker pull vincenzopalazzo/microclang
>> docker run -t -i --rm vincenzopalazzo/microclang bash
>> microcc -help
------------ OUTPUT --------------------
Usage:	microcc [-p|-s|-d|-c] <source_file>

  -p Parse and print AST
  -s Perform semantic checks
  -d Print the generated code
  -o Place the output into file
  -O Optimize the generated code
  -c Compile (default)
  -help  Display this list of options
  --help  Display this list of options
