int main() {
  int a = max(1, 2);
  print(a);
  return 0;
}

// out the scope of the function main
// the compiler throws an error that looks like
// Fatal error: exception Symbol_table.NotFoundInScope
int max(int x, int y) {
  if (x < y) return y;
  return x;
}