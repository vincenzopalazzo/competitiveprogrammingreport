int max(char *x, char *y)
{
	if (x == NULL || y == NULL) {
		return -1;
	}
	if (*x == *y)
		return 1;
	return 0;
}

int main()
{
	print(max(NULL, NULL)); // Invalid syntax, compiler error

	char *null_val = NULL; // Valid sintax, the compiler can 
						 // detect the type of the pointer
	print(max(null_val, null_val)); 
	return 0;
}
