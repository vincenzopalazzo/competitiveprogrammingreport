let fun_definition : (string, (int, Ast.typ) Hashtbl.t) Hashtbl.t =
  Hashtbl.create 0

let ( >-> ) fun_name index type_par =
  let elem = Hashtbl.find_opt fun_definition fun_name in
  match elem with
  | Some v -> Hashtbl.add v index type_par
  | None ->
      Hashtbl.add fun_definition fun_name (Hashtbl.create 0) ;
      let parmas = Hashtbl.find fun_definition fun_name in
      Hashtbl.add parmas index type_par

let ( >? ) fun_name pos =
  logger#debug "Fun name %s -> pos %d " fun_name pos ;
  let params = Hashtbl.find fun_definition fun_name in
  Hashtbl.find params pos
