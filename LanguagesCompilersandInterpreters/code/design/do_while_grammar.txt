and stmt = stmt_node annotated_node

and stmt_node =
  | If of expr * stmt * stmt (* Conditional *)
  | While of expr * stmt (* While loop *)
  | DoWhile of expr * stmt (* Do While loop*)
  | Expr of expr (* Expression statement e; *)
  | Return of expr option (* Return statement *)
  | Block of stmtordec list (* Block: grouping and scope *)
[@@deriving show]
