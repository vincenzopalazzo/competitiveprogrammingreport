// 1. LOWEST
// 2. MEDIUM
// 3. HIGHT
%token <int> INT
%token <char> CHAR
%token <string> ID
%token <bool> BOOL
%token IF ELSE RETURN WHILE FOR DO
%token INT_KEY CHAR_KEY BOOL_KEY VOID NULL
%token LEFT_ROUND "(" RIGHT_ROUND ")"
%token LEFT_BRACKET "[" RIGHT_BRACKET "]"
%token LEFT_BRACE "{" RIGHT_BRACE "}"
%token AMPEREAND "&" COMMA SEMICOLON
%token PLUS MINUS SLASH STAR MOD EQ "=" LT GT
%token LE GE EQEQ NE NOT AND OR
%token EOF PREIN "++" PREDEC "--"
%token ABSUM "+=" ABSUB "-=" ABMOL "*=" ABDIV "/=" ABMOD "%="
