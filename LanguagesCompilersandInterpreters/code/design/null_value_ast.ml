type typ =
  | ..
  | TypNull
[@@deriving show]

and expr_node =
  | ...
  | NullLiteral of typ (* NUll literal *)
[@@deriving show]