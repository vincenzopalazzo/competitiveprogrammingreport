type dec =
  { tipe: Ast.typ
  ; variables: (Ast.identifier, Ast.typ) Hashtbl.t
  ; parent: dec option }
