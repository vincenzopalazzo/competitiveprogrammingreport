type dec =
  { name: string
  ; variables: (Ast.identifier, Llvm.llvalue) Hashtbl.t
  ; parent: dec option
  ; mutable ended: bool }
