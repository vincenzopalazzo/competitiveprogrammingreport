int main() {
  int x = 0;
  x = x + 1;
  x += 1;  // translate from compiler as the line before
  x++;     // increment x, translate as x = x + 1;
  int y;
  y = x++;  // Assign X to y and increment X.

  if (y == (x - 1))  // Return the y mod x
    print(-1);
  return 0;
}