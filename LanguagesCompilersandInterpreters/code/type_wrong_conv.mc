int main() {
  int x = 0;
  char c = 'C';
  x = c;          // This is a invalid assignment in MicroC
  bool flag = x;  // This is a invalid declaration in MicroC
  return 0;
}