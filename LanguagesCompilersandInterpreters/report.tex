\documentclass{article}
\usepackage[final]{hyperref} % adds hyper links inside the generated pdf file
\usepackage[most]{tcolorbox}
\usepackage{csquotes}
\usepackage{float}
\usepackage{caption}
\usepackage{pgffor}
\usepackage{listings}
\usepackage{backnaur}
\usepackage{tabularx}

\hypersetup{
	colorlinks=true,       % false: boxed links; true: colored links
	linkcolor=blue,        % color of internal links
	citecolor=blue,        % color of links to bibliography
	filecolor=magenta,     % color of file links
	urlcolor=blue
}

\newcommand{\sep}{\hspace*{.5em}}
\newcounter{example}
\setcounter{section}{-1}

\def\exampletext{Example} % If English


\NewDocumentEnvironment{testexample}{ O{} }
{
\colorlet{colexam}{red!55!black} % Global example color
\newtcolorbox[use counter=example]{testexamplebox}{%
    % Example Frame Start
    empty,% Empty previously set parameters
    title={\exampletext {} \thetcbcounter: #1},% use \thetcbcounter to access the testexample counter text
    % Attaching a box requires an overlay
    attach boxed title to top left,
       % Ensures proper line breaking in longer titles
       minipage boxed title,
    % (boxed title style requires an overlay)
    boxed title style={empty,size=minimal,toprule=0pt,top=4pt,left=3mm,overlay={}},
    coltitle=colexam,fonttitle=\bfseries,
    before=\par\medskip\noindent,parbox=false,boxsep=0pt,left=3mm,right=0mm,top=2pt,breakable,pad at break=0mm,
       before upper=\csname @totalleftmargin\endcsname0pt, % Use instead of parbox=true. This ensures parskip is inherited by box.
    % Handles box when it exists on one page only
    overlay unbroken={\draw[colexam,line width=.5pt] ([xshift=-0pt]title.north west) -- ([xshift=-0pt]frame.south west); },
    % Handles multipage box: first page
    overlay first={\draw[colexam,line width=.5pt] ([xshift=-0pt]title.north west) -- ([xshift=-0pt]frame.south west); },
    % Handles multipage box: middle page
    overlay middle={\draw[colexam,line width=.5pt] ([xshift=-0pt]frame.north west) -- ([xshift=-0pt]frame.south west); },
    % Handles multipage box: last page
    overlay last={\draw[colexam,line width=.5pt] ([xshift=-0pt]frame.north west) -- ([xshift=-0pt]frame.south west); },%
    }
\begin{testexamplebox}}
{\end{testexamplebox}\endlist}

\newcommand{\chapquote}[3]{\begin{quotation} \textit{#1} \end{quotation} \begin{flushright} - #2, \textit{#3}\end{flushright} }

\renewcommand{\lstlistingname}{Code}% Listing -> Code
\renewcommand{\lstlistlistingname}{List of \lstlistingname s}

%% code section setting definition
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=C,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\begin{document}

\title{MicroC: a subset of C language Stack Based}
\author{Vincenzo Palazzo \\v.palazzo1@studenti.unipi.it}
\date{\today}
\maketitle

\begin{abstract}
   MicroC is an academics compiler of a C subset language full base on the stack.
   It is a complete programming language that contains primitive types and complex
   types as arrays and pointers. It based on LLVM infrastructure, and it is fully developed
   in the Ocaml language.
\end{abstract}
	
	
\section{Intro to MicroC}
\chapquote{``If you think like a computer, write C language make sense"}{Linus Torvalds}{Nothing better than C}


MicroC is an academics compiler of a C subset language full base on the stack, build on top of Low-Level Virtual Machine also known as LLVM \cite{Fandrey},
and it gives the opportunity to support different architecture types like \emph{arm7} and common computer architecture, such as \emph{x86 64}.
As a C language, the MicroC compiler doesn't implement a \enquote{high level} language. This means that is possible access to the memory allocation
with the appropriate operators.
In the following section, there is a general overview of what is possible to do with the MicroC language, and at the end, it is described the 
developing environment with the design choice.

\section{Get Started}

As a good language description before starting it is important to describe how a \enquote{Hello Word} program look like in
MicroC, and also It is a good moment to introduce the very small SDK of the MicroC. In fact, the MicroC include automagically
two methods to read an integer from the standard input and to print an integer to the standard output, and these functions are \emph{getint} and \emph{print}.

\begin{figure}[H]
   \lstinputlisting[language=C, 
      label={code:hello_world}, 
      caption={MicroC hello world.},
      captionpos=b]
      {code/hello_world.mc}
\end{figure}

The Code \ref{code:hello_world} get an integer from the standard input and print this integer on the standard output.
To compiler a MicroC program you need to compile the compiler from the source as described in Section \ref{sec:compilation_process}.

After that you have the possibility to run the script \enquote{microcc.sh} that include the clang compiler to link the 
SDK library and generate a file called \enquote{a.out}, as in the following is shows.
   
\begin{figure}[H]
   \lstinputlisting[frame=none,
      captionpos=b]
      {code/hello_world_result.txt}
\end{figure}


\section{Types, Operators, and Functions}

\subsection{Types}
\label{sec:type}

MicroC supports the basic native type like the C99, so it supports integer, characters, and boolean type, with the only difference that
the type is not equivalent to a language point of view. This means that is not possible to make a cast from a Character to an Integer, 
and there is not equivalence between boolean and integer. So it is not possible to make a cast or insert integer value inside the if condition.
The Code \ref{code:type_wrong_conv} contains an overview of what is legal and what it no legal inside the MicroC.

\lstinputlisting[language=C, 
      label={code:type_wrong_conv}, 
      caption={MicroC invalid program with invalid operation on type.},
      captionpos=b]
      {code/type_wrong_conv.mc}

If this Code \ref{code:type_wrong_conv}is compiled with the MicroC compiler, the following error happens.

\lstinputlisting[frame=none,
   captionpos=b]
   {code/error_microc_type.txt}

MicroC uses the atomic type and doesn't consider any type of conversion (cast) in other types. However, under the hood this was possible 
because in the code generation phases the int, char, and boolean are integer value the following characteristics

\begin{itemize}
\item Integer type (int): It is an integer value of 32 bit, so with MicroC is possible to represent a number contained in $- 2^{31}$ and $2^{31} - 1$, after
         this value, the integer is set to the max value. This behavior is inherited from LLVM, and it is a common problem also for C/C++ 
         language as described in \cite{DietzLi:ICSE12};
   \item Character type (char): Like C/C++ a character type (Char) is an integer of 8 bit.
   \item Boolean type (bool): Like C99/C++ a boolean value is an integer of 1 bit.
\end{itemize}

In addition, MicroC supports also arrays and pointers introduced in the section\ref{sec:arry_pointers}.

\subsection{Operators}
\label{sec:operators}

The basic operation on MicroC is inherited from the C language but it is only on the native type (int, char, or boolean), not pointer operators.
In addition, MicroC supports also the shortcut on the operator\footnote{It is a feature supported in the language not contained in the base version of it.
the feature with the operator is: "Pre/Post increment/decrement" and "Abbreviation for assignment operators".}. 
All the possible operations are summarized in the Code \ref{code:operators_mc}.

\lstinputlisting[language=C, 
      label={code:operators_mc}, 
      caption={MicroC program that contains a summary of type.},
      captionpos=b]
      {code/operators.mc}

The output of the Code \ref{code:operators_mc} is reported below

\lstinputlisting[frame=none,
   captionpos=b]
   {code/operators.txt}

A summary of the Operators is reported in Table \ref{tab:tab_operator}.

\begin{table}[ht]
	\centering
	\begin{tabular*}{\textwidth}{l @{\extracolsep{\fill}} ll}
			\hline
                  MicroC Operator & Example & Meaning \\
			\hline
			$+$, $-$, $/$, $*$, $\%$        & $a * b$               & Common Math operator \\
                  $++$, $--$                      & $a++$, $i = i++$      & Pre and Post increment/decrement\\
                  $+=$, $-=$, $/=$,\\ $*=$, $\%=$ & $a += 1$              & Shortcut to write $a = a + 1;$\\
                  \hline
	\end{tabular*}
	\caption{Table to report all the operator on basic type supported by MicroC language.}
	\label{tab:tab_operator}
\end{table}

As reported in Table \ref{tab:tab_operator} MicroC support also operators like Post and Pre increment/decrement and an example of code is 
reported in the Code \ref{code:operators_mc}. In addition, the language supports also an abbreviation for assignment operators such as the common C.

\subsection{Function}
\label{sec:function}
 
A program in MicroC need to start to read from the button to the top because only the function on the bottom can see the function on the top of
the program.
In this section is described the function and their scope in the language that is too close to the C language but there are missing proprieties, such as 
function definition.

A function in MicroC contains a type return that can be only a basic type introduced in the Section\ref{sec:type}, a name, and a list of parameters. The 
Code \ref{code:function_dec} shows a simple function declaration.

\lstinputlisting[language=C, 
      label={code:function_dec}, 
      caption={MicroC program that contains of function definition.},
      captionpos=b]
      {code/function_dec.mc}

The output of this Code \ref{code:function_dec}is reported below.

\lstinputlisting[frame=none,
         captionpos=b]
         {code/function_dec.txt}

How the Code \ref{code:function_dec}shows, the function \emph{max} is declared on the top of the function \emph{main}, the motivation 
is the scope of the function
main know the function max because during the analysis the compiler put inside a symbol table (see Section \ref{sec:language_design}) 
the function declaration max.
However, if the function max is declared to the bottom of the function main, the MicroC compiler throws an exception and the 
Code \ref{code:wrong_function_dec} shows a code example.

\lstinputlisting[language=C, 
      label={code:wrong_function_dec}, 
      caption={MicroC program that contains a summary of type.},
      captionpos=b]
      {code/wrong_function_dec.mc}

In addition, the parameters of the function can be pointers or array as described in Section\ref{sec:arry_pointers}.

\subsection{Variable}
\label{sec:label}

There are two types of variable in MicroC, the \emph{global variable} and the \emph{local variable}, where the global variable are located out of the function
and the visibility is like the function.\\
In fact, the visibility of global variable stars from the declaration point to the bottom of the program, such as the function visibility.\\

The Code \ref{code:hello_global_ptr1} shows an example of Global variable usage

\lstinputlisting[language=C, 
      label={code:hello_global_ptr1}, 
      caption={MicroC program that contains a summary of type.},
      captionpos=b]
      {code/hello-global-ptr1.mc}

The output of the Code \ref{code:hello_global_ptr1} is reported below.

\lstinputlisting[frame=none,
         captionpos=b]
         {code/hello-global-ptr1.txt}

An example of out of scope declaration is reported with the Code\ref{code:wrong_global_ptr1}.

\lstinputlisting[language=C, 
      label={code:wrong_global_ptr1}, 
      caption={MicroC program that contains a summary of type.},
      captionpos=b]
      {code/wrong_global_ptr1.mc}

\subsection{Array and Pointers}
\label{sec:arry_pointers}
      
\subsection{Array}

The array in MicroC has the same characteristic as the Array in C, so the array as function parameters
are a pointer to the first element of the array. In addition, the array is a continuous element in the memory,
this behavior is inherited from LLVM.

An example of array declaration is shown in the Code \ref{code:example_array}

\lstinputlisting[language=C, 
      label={code:example_array}, 
      caption={MicroC program that contains a summary of type.},
      captionpos=b]
      {code/example_array.mc}

In the Code \ref{code:example_array} is reported an array as parameter function, in this case, the Array is managed from the compiler as a 
pointer, and also inside the function under the hood is managed as a pointer.

\subsection{Pointers}

With MicroC is possible to manage a pointer but it is not possible to create it directly with a call to \emph{malloc} or destroy a pointer
with a function call like \emph{free} in C.
To get a pointer with MicroC is needed to use the dereferencing operator \&, this operator has the same semantic of the C language and a complete 
summary of the pointer is shown in the Code \ref{code:example_ptr}.

\lstinputlisting[language=C, 
      label={code:example_ptr}, 
      caption={MicroC program that contains a summary of pointers usage.},
      captionpos=b]
      {code/example_ptr.mc}

As shown in the Code\ref{code:example_ptr}, to get the value of the pointer is used the referencing operator \emph{*} as C language.
In addition, the language supports function call by reference, in fact, is possible to write a code like the 
Code \ref{code:example_reference}to increment a value with a helper function.

\lstinputlisting[language=C, 
      label={code:example_reference}, 
      caption={MicroC program that contains an example of call to a function by references value.},
      captionpos=b]
      {code/example_reference.mc}

The output Code \ref{code:example_reference} is shown below.

\lstinputlisting[frame=none,
         captionpos=b]
         {code/example_reference.txt}

In conclusion, language supports a special pointer, call \emph{NULL}, this type of pointer is very useful because help to initialize a
value of a pointer to a NULL, that is mean that the value is unknown. The meaning of this pointer is equal to nullptr in C++ and undefined in 
JavaScript (ES), but it is really different from the NULL value in C because the null value in C is defined as reported in the Code\ref{code:null_def}.

\lstinputlisting[language=C, 
      label={code:null_def}, 
      caption={NULL definition of C language.},
      captionpos=b]
      {code/null_def.h}

In the first release of the compiler (v0.1), it contains a bug and it is described in Section \ref{sec:design_error} as an error during the design 
of the language.
\section{Control Flow and loops}
As all modern language, MicroC supports the control flow and loops as for, while, and do-while, but it does not
support another statement as a switch case. As in C language, MicroC support stamens with curly brackets and without
curly brackets, and when the curly brackets are not included, only the statement after the declaration is considered
under the scope of the control flow or loop statement. 

\subsection{If-Else}

The only difference with the if the statement is the limitation of the type that is possible to insert inside the condition, 
in fact, the condition must be a boolean value, the strict type system of MicroC doesn't permit to use of an integer or
a NULL pointer as a condition.

\lstinputlisting[language=C, 
      label={code:example_if_stm}, 
      caption={MicroC program that contains an example of if statement declaration.},
      captionpos=b]
      {code/example_if_stm.mc}

The output of the Code \ref{code:example_if_stm} is reported below 

\lstinputlisting[frame=none,
         captionpos=b]
         {code/example_if_stm.txt}

\subsection{Loops - While, and For}

The loops in MicroC are defined as the loops in C in c language, with the only limitation on the condition declaration, 
the condition as in the if statement needs to be a boolean type.
In this section, the loops are presented by examples

\subsubsection{While}

\lstinputlisting[language=C, 
      label={code:example_while}, 
      caption={MicroC program that contains an example of while loop declaration.},
      captionpos=b]
      {code/example_while.mc}

The output of the Code \ref{code:example_while} is reported below 

\lstinputlisting[frame=none,
         captionpos=b]
         {code/example_while.txt}

\subsubsection{Do While}

This statement is one of the additional feature support by MicroC, the original language doesn't have it,
but it is an additional feature to add inside the language.
An example of statement usage is reported in the Code \ref{code:example_do_while}.

\lstinputlisting[language=C, 
      label={code:example_do_while}, 
      caption={MicroC program that contains an example of do while loop declaration.},
      captionpos=b]
      {code/example_do_while.mc}

The output of the Code \ref{code:example_do_while} is reported below 

\lstinputlisting[frame=none,
         captionpos=b]
         {code/example_do_while.txt}

\subsubsection{For loop}
\label{sec:for_loop}

Under the hood, the \emph{for loop} statements is managed as the while statement, with the simple trick to move the
declaration expression out the statement and the increment statement at the end of the loop declaration.

The Code \ref{code:example_for} shows a complete summary to utilized the for loops in MicroC.

\lstinputlisting[language=C, 
      label={code:example_for}, 
      caption={MicroC program that contains an example of if statement declaration.},
      captionpos=b]
      {code/example_for.mc}

The output of the Code \ref{code:example_for} is reported below 

\lstinputlisting[frame=none,
         captionpos=b]
         {code/example_for.txt}

The example \ref{code:example_for} shows different types of usage of for loop, In fact, MicroC language support
the for as defined in C89 standard (used in the function insertion sort of the example), and a modern for declaration
in a modern version of C (version greater than 89)
shows in the function print array. In conclusion, the for statement is defined as a three expression separated by a semicolon. these 
the expression is optional, so I can write also some code that looks like the Code\ref{code:example_for_opt}.

\lstinputlisting[language=C, 
      label={code:example_for_opt}, 
      caption={MicroC program that contains an example of if statement declaration.},
      captionpos=b]
      {code/example_for_opt.mc}

The Code \ref{code:example_for_opt} shows a method that is used to create an infinite loop with a for a statement, it is equivalent to a while with a
condition equal to true in each moment of the execution ( while(true) ).

\section{Language Design}
\label{sec:language_design}
In this section is described the design language, in particular, what are the choice taken during the developing process.
However, there are some errors in the design language described in Section \ref{sec:design_error}.

The first part of the compiler is the front end part, which includes the scanner developed with the ocamellex and the parser developer with Menhir, which is an LR(1) parser generator. In this phases the source code from the
file .mc is analyzed by the scanner and it breaks the source code in TOKEN and an example of the token are reported 
in the Code \ref{code:tokens_ex}.

\lstinputlisting[frame=none,
            label={code:tokens_ex}, 
            caption={MicroC tokens created by the Scanner.},
            captionpos=b]
         {code/design/tokens_ex.txt}

These tokens are being created with the scanner and after, they are analyzed with a Parser, where the grammar rules are expressed. 
%and we can summarize the grammar with the following expression

%\begin{bnf*}
%  \bnfprod{list}
%    {\bnfpn{listitems} \bnfor \bnfes}\\
%  \bnfprod{listitems}
%    {\bnfpn{item} \bnfor \bnfpn{item}
%     \bnfsp \bnfts{;} \bnfsp \bnfpn{listitems}}\\
%  \bnfprod{item}
%    {\bnftd{description of item}}
%\end{bnf*}

The parser will generate an Abstract Syntax Tree, and in this phase are implemented new nodes to support the additional feature of the language.
In fact, the example reported in the previous section contains additional features from the original MicroC language. The features are summarized in
the Table \ref{tab:table_feature}.

\begin{table}[ht]
      \centering
      \begin{tabular*}{\textwidth}{l @{\extracolsep{\fill}} ll}
                  \hline
                  Feature & Example   \\
                  \hline
                  Do-while loops                           & do \{ \} while(true);  \\
                  Pre/Post increment/decrement operators   & i++; int a = i++;  \\
                  Abbreviation for assignment operators    & i += 10; i\%=2     \\
                  Variable declaration with initialization & int i = 0;         \\
                  \hline
      \end{tabular*}
      \caption{Table that contains all the MicroC feature added to the language.}
      \label{tab:table_feature}
\end{table}

To add these feature is needed to modify the Abstract Syntax Tree, In particular, to support the do-while statement is need to add a new node, and 
in these case, the choice is to create a new Statement \emph{stmt} and it is shown in the Code\ref{code:do_while_grammar}.

\lstinputlisting[frame=none,
            language=caml,
            label={code:do_while_grammar}, 
            caption={Portion of MicroC grammar to implement the do-while stamens.},
            captionpos=b]
         {code/design/do_while_grammar.txt}

Also for other features, another element in the Abstract Syntax tree is added, such as the Pre/Post increment/decrement operators are implemented
as a unary operation, where the unary operation is defined as a type to represent the operation and an expression. The Code \ref{code:pre_post_increment} 
shows how these operations are implemented in the grammar.

\lstinputlisting[frame=none,
            language=caml,
            label={code:pre_post_increment}, 
            caption={Portion of MicroC grammart to implement Pre/Post increment/decrement operators.},
            captionpos=b]
         {code/design/pre_post_increment.txt}

In addition, the parser rules are summarized in the Code\ref{code:parser_pre_post_increment}.

\lstinputlisting[frame=none,
            language=caml,
            label={code:parser_pre_post_increment}, 
            caption={Portion of parser code to parserize Pre/Post increment/decrement operators.},
            captionpos=b]
         {code/design/parser_pre_post_increment.txt}

To conclude the discussion about the feature support it is important to introduce the inline declaration of a variable, it is an important feature 
of the language and it is introduced to declare the global variable, the local variable and as it is shown in the Section \ref{sec:for_loop} it is  supported 
also inside the For loop declaration.

The grammar declaration is shown inside the Figure \ref{code:pre_post_increment} and the node is called \emph{DecAssig} to declare the local variable
and the parser rules are declared as an expression as reported in the Figure \ref{code:parser_pre_post_increment}.

After this brief discussion on the grammar, the next step of the language is the static analysis, which in MicroC it is important because 
the language is without type coercion.
In fact, MicroC to implement the static analysis use a Symbol Table implementation where it stores a tuple formed from the name of 
the variable or function and the
type of it.

After the parsing, an abstract syntax tree is created, but it is not a safe typed, in fact, after the parser faces it is possible to have some 
inconsistent assignment or some declaration without any meaning.
To make the semantic analysis MicroC use a simple algorithm that scans the Abstract Syntax Tree and store the declaration found in a Symbol Table. 
The Symbol Table for MicroC is a Linked List implemented like the Code \ref{code:symbol_table_semantic}.
         
\lstinputlisting[frame=none,
            language=caml,
            label={code:symbol_table_semantic}, 
            caption={Type declaration Symbol Table for semantics analysis.},
            captionpos=b]
         {code/design/symbol_table_semantic.ml}

How shows in the Code \ref{code:symbol_table_semantic} the implementation of the Symbol table is a statement declaration, where the statement represents the scope of the statement. This scope can be subdivided into "Global scope", 
"Function scope" and the statement declaration scope, as loops. In addition,
the scope has a type to make the consistency check in the return type.
For example, if the function is a void type it does not possibly return an integer value, and the propriety \emph{tipe} in the Symbol Table declaration help
to remember the type of the declaration during the analysis of the program.

In conclusion, after the semantic analysis phase, the last phase is the code generation, which use the LLVM to make a \emph{Intermediary Representation} of
the MicroC code, and it is the last phases of the compiler before generating a byte-code file called \emph{a.bc} as described 
in the section \ref{sec:compilation_process}.
During the code generation phases, MicroC uses another Symbol Table to memorize the LLVM value generated, and it helps to make another scope visibility check 
that should never happen inside the compiling process. The Symbol Table used during the code generation phases has a different 
implementation and it is reported in
the Code \ref{code:symbol_table_codegen}.

\lstinputlisting[frame=none,
            language=caml,
            label={code:symbol_table_codegen}, 
            caption={Type declaration Symbol Table for semantics analysis.},
            captionpos=b]
         {code/design/symbol_table_codegen.ml}

The implementation of the Symbol Table during the code generation is different from the semantic analysis as shows in the Code \ref{code:symbol_table_codegen},
the main motivation is because it is different requires in the two faces. In particular, we can observe that we have in the name of the scope, that it can be
the name of the function or the name of the statement, such as if, while. This propriety is needed because during the execution could happen that can be nested
statement such as nested if, loops, and during the code generation LLVM need the function declaration llvalue\footnote{A llvalue is a LLVM type that represents each type generated for IR generation. A value can be a function, an integer, or a variable. For LLVM all thing is a llvalue} generated to a new basic block
\footnote{A basic block represents a single entry single exit section of code. Basic blocks contain a list of instructions that form the body of the block.}
to another basic block generated at some point before the execution.

To conclude the discussion of the Symbol Table is important to introduce the  boolean propriety called ended in the Symbol table is a flag that is used to check if the main function contains two or more 
return for the same scope, this could be a semantic error but for MicroC as C language it is a valid state of the program with the only exception 
that consists to ignore all the code after the first return.

In conclusion, another hash map is used in the Semantic Check to track the type of function parameter, and the implementation is reported in the Code \ref{code:functio_hash_amp}.

\lstinputlisting[frame=none,
            language=caml,
            label={code:functio_hash_amp}, 
            caption={Hash Map declaration to maintains the track of function parameter.},
            captionpos=b]
         {code/design/functio_hash_amp.ml}

The implementation of the Hash Map is required because the function call can happen in each moment of the program life and the Symbol Table maintains 
the data of the scope that the algorithm is analyzing in a particular moment of the analysis and after that, the scope is destroyed. In addition, 
how the Symbol table is implemented is difficult to get the global scope, because it sometimes can require $\mathcal{O}(N)$ to get the global scope 
inside the Linked List. 
For this reason, the additional Hash map is useful to access directly to the function call that I need to make the check. The cost of the find function
in this case, is $\mathcal{O}(1)$

\section{Language Design Error}
\label{sec:design_error}
\chapquote{“Good design is like a refrigerator—when it works, no one notices, but when it doesn’t, it sure stinks.”}{Irene Au}{UI Designer}

The development of the MicroC compiler was a challenge because it is a new field and I need to make some choices during the development
without knowing what is exactly the next step to do. In particular, one of the big challenges is to design the semantic check without know
well what are the rules of LLVM Ocaml binding and this brings some error during the design. One big bug in MicroC is the NULL value.
By design it was implemented in the Abstract Syntax Tree it is a literal call Null Literal and it has a type that is new type calls Null Type, and
the Code \ref{code:null_value_ast} shows the implementation.

\lstinputlisting[frame=none,
            language=caml,
            label={code:null_value_ast}, 
            caption={MicroC NULL value represented in the Abstract Syntax Tree.},
            captionpos=b]
         {code/design/null_value_ast.ml}

As represented in the Abstract Syntax Tree the NULL value can have a different basic type but the MicroC compiler doesn't have an algorithm to change the value 
inside the Abstract Syntax Tree. For this reason, it is not possible to change the type with the correct type.\\
One possible solution is to add an additional feature to Semantic Analysis, and a new Abstract Syntax Tree typed.\\ However, this solution
is considered a future feature to add and it is not available on the first version of the compiler.\\
The main motivation of this bug is the meaning of NULL value in MicroC, it is considered a special pointer, and during the phases of code generation, 
LLVM wants a lltype\footnote{Lltype represent the type of a value.} to declare a null pointer.\\
This lltype is necessary because the pointer is not a constant but can change during the life of the program and for this reason, LLVM wants a pointer of the
same type.

MicroC implement a solution for this bug and support the pointer of all the type, but this makes the code more complex and unreadable, for this reason, 
some cases are avoided.
For example, it is not possible to pass a NULL value as a function parameter inside a function call, but to pass a null value to a function is needed 
a trick that consists to create a variable before and after pass this variable to a function call.
The Code \ref{code:design_error} summarizes the cases.

\lstinputlisting[frame=none,
            language=C,
            label={code:design_error}, 
            caption={MicroC example to pass a NULL value to a function parameter.},
            captionpos=b]
         {code/design/design_error.mc}

\section{Compilation Process}
\label{sec:compilation_process}

To the compiler, a MicroC program is needed a binary version of the MicroC compiler, and to obtain the binary version of it
is needed to compile the source code that can be download to \cite{github:microclang} or it is possible use the docker image available 
on Docker Hub \cite{docker:microclang}\footnote{The docker image is uploaded without source code, it contains only the file necessary 
to run the compiler and compiler a MicroC program.}.

To compiler, the source code is needed to install all the OCaml libraries with opam. In addition, there is an additional library
that is needed from the original version of MicroC, that is a logger library calls Easy Logging \cite{github:easylogging}. In
particular, is used version 0.7.1.

A list of dependencies are reported below

\begin{itemize}
      \item ocamlbuild v0.14.0;
      \item ocamlformat v0.16.0;
      \item llvm v10.0.0 Strictly required;
      \item menhir v20201216;
      \item easy logging v0.7.1;
      \item ppx deriving v5.2.1.
\end{itemize}

It is also required the LLVM 10 version and the procedure to install it is described on the official site 
\href{https://releases.llvm.org/download.html}{https://releases.llvm.org/download.html}

An example of the Install procedure is also on the README.md of the source code, and it is reported below

\lstinputlisting[frame=none,
         captionpos=b]
         {code/install_procedure.txt}

In conclusion, it is possible to build the docker image with docker-compose locally with a simple command \emph{docker-compose up} in the root
project directory. 
It is also possible to pull the docker image from the docker registry to run the MicorC compiler and the command are reported below

\lstinputlisting[frame=none,
         captionpos=b]
         {code/docker_image_example.txt}

After the dependency install\footnote{It required that Python 3 is installed on the system that are running the test}, it is possible use the make build automation tool to make build the MicroC compiler, and all the commands are reported
below.

\begin{itemize}
      \item make: Run ocamlbuild to link all ocamel dependencies and compiler the source code;
      \item make clean: Remove all the build results, and clean the directory.
      \item make check: Run the python test, it required the python3 and additional dependencies that is possible install with the command reported below.
      \item make install: Install the microcc compiler in the UNIX system.
      \item make uninstall: Uninstall the microcc compiler from the UNIX system.
\end{itemize}

The source code contains a complete battery of test unit developed with Python 3.6 and pytest, and the command to install the dependencies and the test are 
reported below

\lstinputlisting[frame=none,
         captionpos=b]
         {code/microcc_tests.txt}

The introduction of docker and python help to make a complete pipeline on GitLab to make the regression testing in each change and in a clean environment each
time, this helps to make the development of the compiler safer and avoid regression during the change inside the source code.

\bibliography{ref} 
\bibliographystyle{ieeetr}

\end{document}