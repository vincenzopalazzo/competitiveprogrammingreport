% !TeX TS-program = xelatex

\documentclass[aspectratio=43,11p, handout]{beamer}
%\documentclass[aspectratio=43,11p, handout]{beamer} % compile version without pause
%\documentclass[aspectratio=169,11p]{beamer}

\usetheme{metropolis}           % Use metropolis theme

\usepackage{FiraSans}
\usepackage[many]{tcolorbox}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{listings}
\usepackage{color}
\usepackage{hyperref}
\usepackage[framemethod=tikz]{mdframed}
\usepackage{fontawesome5}

\definecolor{cccolor}{rgb}{.67,.7,.67}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\graphicspath{ {./images/} }
\captionsetup{labelformat=empty}
\setsansfont[
    Extension      = .otf,
    UprightFont    = *-Light,
    ItalicFont     = *-LightItalic,
    BoldFont       = *-Regular
]{FiraSans}
\setmonofont[
    Extension   = .otf,
    UprightFont = *-Regular,
    BoldFont    = *-Medium
]{FiraMono}

\newcommand{\adv}[1]{ {\color{gray} #1} }

\newcommand{\SubItem}[1]{
    {\setlength\itemindent{15pt} \item[-] #1}
}

\title{1/3: Introduction to Graph Database}
\date{\today}
\author{Vincenzo Palazzo v.palazzo1@studenti.unipi.it}
\institute{University of Pisa}

\setbeamertemplate{section in toc}{\inserttocsectionnumber.~\inserttocsection}

\begin{document}
  \maketitle
  
  \begin{frame}{Table of Content}
    \tableofcontents
  \end{frame}


  \section{Recall to Graphs}
  
  \begin{frame}{Recall to Graphs}
    \begin{itemize}
    \item A Graph is a generic data structure, formed by vertices and edges;
    \item \pause It is a data structure used to model the relationship between two vertices; 
    \item \pause This flexibility gives the possibility to model in an easy way a big set of 
    real works scenarios.
    \end{itemize}
  \end{frame}

  \begin{frame}{Real word usage of Graphs}
    \begin{itemize}
    \item Medical history;
    \item \pause Web pages dependencies, and pages visited by users; 
    \item \pause Tracking of Bitcoin transactions.
    \end{itemize}
  \end{frame}

  \begin{frame}{Real world example}
    \begin{itemize}
      \item Graph representation of Bitcoin blockchain.
    \end{itemize}
    \pause {
      \begin{figure}
        \includegraphics[width=11cm]{Selection_056.png}
      \end{figure}
      \begin{itemize}
        \item \pause Source: \url{https://vincenzopalazzo.github.io/SpyJSBlock.react}.
      \end{itemize}
    }
  \end{frame}

  \begin{frame}{Real world example}
    \begin{itemize}
      \item Relationship between addresses in the Bitcoin blockchain.
    \end{itemize}
    \pause {
      \begin{figure}
        \includegraphics[width=11cm]{naturalAddressGrahScamTx_correcter.png}
      \end{figure}
    }
  \end{frame}

  \section{What is a Graph Database}
  \begin{frame}{What is a Graph Database}
    \begin{itemize}
      \item It is a database system with CRUD operation that exposes a Graph data model;
      \item \pause It is generally used with transactional systems;
      \item \pause There are different types of Graph databases, with different properties.
    \end{itemize}
  \end{frame}

  \begin{frame}{Type of Graph Database}
    \begin{itemize}
      \item {\bf Native graph database}: Use the \emph{index-free adjacency} to store and process the graph data model;
      \item \pause {\bf Not Native Storage}: Implementing not native storage and not native process engine.
    \end{itemize}
    \pause It can't be said which type of Graph Database is the best to be chosen, cosidering that the choice involves classic engineering trade-offs
  \end{frame}

  \begin{frame}{Important Graph Database Properties}
    \begin{itemize}
      \item {\bf Process engine}: Used to process the graph data structure;
      \item \pause {\bf Underlying Storage}: Used to store the graph data structure;
    \end{itemize}
  \end{frame}

  \begin{frame}{Underlying Storage}
    \begin{itemize}
      \item {\bf Native Storage}: Store the data as graph of relationship, 
            this depends from what implementation it is used;
      \item \pause {\bf Not Native Storage}: The graph database is usually layered on top of other databases.
    \end{itemize}
  \end{frame}

  \begin{frame}{Process engine}
    \begin{itemize}
      \item {\bf Native process engine}: It requires a method to store and access connected nodes in a fast way, 
            usually through a \emph{index-free adjacency};
      \item \pause {\bf Not Native process engine}: Uses the process engine offers through the underlying database.
    \end{itemize}
  \end{frame}

  \section{Graph Database examples}
  \begin{frame}{Graph Database examples}
    The most famous open source Graph Databases\footnote{All the data are taken in October 2021} are:
    \pause {
      \begin{itemize}
        \item {\bf Neo4J}: 9.4k starts on Github \url{https://neo4j.com}
        \pause {
          \begin{itemize}
            \item It is a Native Graph database written in Java.
          \end{itemize}
        }
        \item \pause {\bf ArangoDB}: 11.7k starts on Github \url{https://www.arangodb.com}
        \pause {
          \begin{itemize}
            \item It is a Multi model Graph database written in C++.
          \end{itemize}
        }
        \item \pause {\bf RedisGraph}: 1.5k starts on Github \url{https://oss.redis.com/redisgraph}
        \pause {
          \begin{itemize}
            \item It is a native Graph database written in C that use sparse metrics to store the data.
          \end{itemize}
        }
      \end{itemize}
    }
  \end{frame}

  \begin{frame}{Graph Database Overview}
    \begin{figure}
      \includegraphics[width=10.5cm]{overview.jpeg}
    \end{figure}
  \end{frame}

  \begin{frame}[standout]
    \begin{mdframed}[outerlinecolor=black,outerlinewidth=2pt,linecolor=cccolor,middlelinewidth=3pt,roundcorner=10pt]
      This work is licensed under a Creative Commons Attribution 3.0 Unported License.
      \faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa
    \end{mdframed}
  \end{frame}
\end{document}
