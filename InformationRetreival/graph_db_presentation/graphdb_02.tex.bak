% !TEX program = xelatex

\documentclass[aspectratio=43,11p, handout]{beamer}
%\documentclass[aspectratio=43,11p, handout]{beamer} % compile version without pause
%\documentclass[aspectratio=169,11p]{beamer}

\usetheme{metropolis}           % Use metropolis theme

\usepackage{FiraSans}
\usepackage[many]{tcolorbox}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{color}
\usepackage[framemethod=tikz]{mdframed}
\usepackage{fontawesome5}

\definecolor{cccolor}{rgb}{.67,.7,.67}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{
  basicstyle=\fontsize{8}{13}\selectfont\ttfamily
}

\graphicspath{ {./images/} }
\captionsetup{labelformat=empty}
\setsansfont[
    Extension      = .otf,
    UprightFont    = *-Light,
    ItalicFont     = *-LightItalic,
    BoldFont       = *-Regular
]{FiraSans}
\setmonofont[
    Extension   = .otf,
    UprightFont = *-Regular,
    BoldFont    = *-Medium
]{FiraMono}

\newcommand{\adv}[1]{ {\color{gray} #1} }
\newcommand{\quotes}[1]{``#1''}

\title{2/3: Introduction to Graph DB}
\date{\today}
\author{Vincenzo Palazzo v.palazzo1@studenti.unipi.it}
\institute{University of Pisa}

\setbeamertemplate{section in toc}{\inserttocsectionnumber.~\inserttocsection}

\begin{document}
  \maketitle
  
  \begin{frame}{Table of Content}
    \tableofcontents
  \end{frame}

  \section{Power of Graph Database}

  \begin{frame}{Power of Graph Database}
    \begin{itemize}
      \item Graph database provides a powerful but novel data modeling technique;
      \item \pause These type of speedup is given by the Index-free adjacency concept;
      \item \pause In addition, they offer an extremely flexible data model.
      \item \pause However, they don't provide sufficient justification for replacing 
            a well-established data platform, such as Relational Database, No-SQL Database;
    \end{itemize}
  \end{frame}

  \section{Index-free adjacency}
  
  \begin{frame}{Why we need Index-free adjacency?}

    \begin{itemize}
      \item \pause Relational database, are not very good with some real world relationship (ironically).
      \item \pause Relational do exist in the all relational databases, but only as a means of
                   joining tables;
    \end{itemize}

  \end{frame}

  \begin{frame}{Why we need?}
    \begin{figure}
        \includegraphics[width=11cm]{db_join.png}
    \end{figure}
  \end{frame}

  \begin{frame}{Why we need Index-free adjacency?}
    \begin{itemize}
      \item Index-free adjacency use that idea to store the reference of who is in relationship with what, and
      \item \pause It is possible to have a big speedup in terms of performance;
    \end{itemize}
  \end{frame}

  \begin{frame}{Index-free adjacency vs Relational Databases}
    \begin{itemize}
      \item An analogy to explain the differences:
      \item \pause In the Relation Database:
      \pause {
        \begin{figure}
          \includegraphics[width=7cm]{walk_index.png}
        \end{figure}
      }
      \item \pause With an Index-free adjacency:
      \pause {
        \begin{figure}
          \includegraphics[width=7cm]{walk_free.png}
        \end{figure}
      }
    \end{itemize}
    \pause { Source: \url{shorturl.at/ezBF9} }
  \end{frame}

  \begin{frame}{Index-free adjacency vs Relational Databases performance}
    \begin{itemize}
      \item The Index-free adjacency are used in the native graph database, like Neo4j;
      \item \pause The following figure reports a comparison of performance between a 
      Neo4j and a relational database during a graph traversal operation;
      \pause {
        \begin{figure}
          \includegraphics[width=9cm]{comprarison_db.png}
        \end{figure}
      }
      \item \pause How we can see there are situations, where a native graph it is better that a not native graph;
    \end{itemize}
    \pause { Source: \url{shorturl.at/KQ158} }
  \end{frame}

  \section{Data Model with Graphs}

  \begin{frame}{Data Model with Graphs}
    \begin{itemize}
      \item How do we model the world in terms of graphs?
      \item \pause Abstract the important details from a domain using circles and boxes;
      \item \pause Describe the connections between these things by joining them with arrows.
    \end{itemize}
  \end{frame}

  \begin{frame}{Data Model with Graphs}
    \begin{itemize}
      \item How we can describe the data model?
      \item \pause Graph database has no standard language (yet) like SQL;
      \item \pause In fact, each graph has its own language to describe the models;
      \item \pause These languages, are used to describe the data models, and to query the database;
      \item \pause One popular language is called Cypher, and it is used in Node4j;
    \end{itemize}
  \end{frame}

  \section{An Introduction to Cypher}

  \begin{frame}{An Introduction to Cypher}
    \begin{itemize}
      \item It is designed to be easily read; 
      \item \pause It is open-source with a own specification available at \url{https://github.com/opencypher/openCypher};
      \item \pause It is a good starting point to learn other graph query languages;
      \item \pause It gives the possibility to express exactly what the user wants retreival from the db.
    \end{itemize}
  \end{frame}

  \begin{frame}{An Introduction to Cypher}
    \begin{itemize}
      \item Cypher describes the data model based on how things look like, and draw them, using ASCII art.
      \item \pause An example of pattern with Cypher syntax it is the following one:
        \begin{center}
          (a)-[:KNOWS]->(b)-[:KNOWS]->(c), (a)-[:KNOWS]->(c)
        \end{center}
      \pause {
        \begin{figure}
          \includegraphics[width=6cm]{graph_ex.png}
        \end{figure}
      }
    \end{itemize}
  \end{frame}

  \begin{frame}[fragile]
    \frametitle{Example}
    \begin{itemize}
      \item Cypher is composed of clauses.
      \item \pause A simplest queries consist of a START clause followed by a MATCH and a RETURN clause:
        \begin{lstlisting}
START a=node:user(name='Michael')
MATCH (a)-[:KNOWS]->(b)-[:KNOWS]->(c), (a)-[:KNOWS]->(c)
RETURN b, c
        \end{lstlisting}
    \end{itemize}
  \end{frame}

  \begin{frame}[fragile]
    \frametitle{Example}
    \begin{itemize}
      \item Cypher gives the possibility to drive the query;
      \item \pause In fact, we can identify a node between rounded parenthesis, such as $(a)$;
      \item \pause A node can have a filtering by type with the following convention $(a:User)$;
      \item \pause In this way, we are specifing that we want a node that is of "User" type, 
      and we reference it in the query as "a";
      \item \pause Finally we can filter the node by property, with the following convention $(a:User \{name: "Michael"\})$
    \end{itemize}
  \end{frame}

  \begin{frame}[fragile]
    \frametitle{Example}
    \begin{itemize}
      \item Cypher has a similar definition for the relationship (verteces), identified by the $-->$ or $-[s:School]->$;
      \item \pause The language enables the same declarative operation on the node, also for the verteces.
      \item \pause {
        \begin{lstlisting}
MATCH
  (p1:Person)-[:DRIVERS]->(c:Car)-[:OWNED_BY]-> (p2:Person),
  (p2:Person) <-[:LOVES]-(p1)
RETURN p1
        \end{lstlisting}
        }
    \end{itemize}
  \end{frame}

  \begin{frame}{Closes in detail}
    \begin{itemize}
      \item {\bf START}: specifies one or more starting nodes or relationships in the graph;
      \item \pause {\bf MATCH}: Describes a pattern that needs to be searched in the graph;
            \begin{itemize}
              \item This pattern could, in theory, occur many times throughout our graph data
            \end{itemize}
      \item \pause {\bf RETURN}: This clause specifies which nodes, relationships, and properties in the matched data
      should be returned.
      \item \pause Others closes are: {\bf WHERE}, {\bf CREATE}, {\bf CREATE UNIQUE}, {\bf DELETE}, 
                    {\bf SET}, {\bf FOREACH}, {\bf UNION}, {\bf WITH}.
    \end{itemize}
  \end{frame}

  \begin{frame}[standout]
    \begin{mdframed}[outerlinecolor=black,outerlinewidth=2pt,linecolor=cccolor,middlelinewidth=3pt,roundcorner=10pt]
      This work is licensed under a Creative Commons Attribution 3.0 Unported License.
      \faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa
    \end{mdframed}
  \end{frame}
\end{document}
