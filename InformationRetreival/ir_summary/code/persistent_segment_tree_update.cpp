void update(int at, T value) {
  auto new_root = update_range_subroutine(root, 0, origin.size(), at, value);
  history.push_back(new_root);
  root = new_root;
}