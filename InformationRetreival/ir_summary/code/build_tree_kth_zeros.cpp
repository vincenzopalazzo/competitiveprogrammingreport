void build_structure_procedure(std::size_t start_index, std::size_t left_index,
                               std::size_t right_index) {
  if (left_index == right_index) {
    if (origin[left_index] == 0)
      structure[start_index] = 1;
    else
      structure[start_index] = 0;
    return;
  }
  auto middle_point = (left_index + right_index) / 2;
  auto left_child = left_child_index(start_index);
  auto right_child = right_child_index(start_index);
  build_structure_procedure(left_child, left_index, middle_point);
  build_structure_procedure(right_child, middle_point + 1, right_index);
  // Internal node will have the sum of both of its children
  auto segment_left = structure[left_child];
  auto segment_right = structure[right_child];
  structure[start_index] = segment_left + segment_right;
}