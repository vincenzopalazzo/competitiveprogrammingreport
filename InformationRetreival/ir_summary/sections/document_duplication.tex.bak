
\section{Document Duplication}
\chapquote{``You are not here to duplicate and replicate. You are here to initiate."}{Alan Cohen}{}
    The web is full of duplicated content, in particular, it is interesting to catch near duplicated, where
    the web page contains only small changes from the previous. To find exactly duplicate we can use hash functions,
    like cryptographic hash function or hash function like Karp Rabin Fingerprint.

    \subsection{Karp Rabin Fingerprint}
      Checking that two big element are the same can be expensive, but if we check the Fingerprint of these two
      elements the check on the equality can be more easy.

      A good fingeriprint need to have the following propriety:
      \begin{itemize}
          \item Low probability of collision;
          \item Fast computation and comparison time.
      \end{itemize}

      The Karp Rabin Fingerprint is a good way to calculate the fingerprint, it is called also rolling hashing, and the 
      basic idea under this hash function is te following one:

      \begin{itemize}
          \item Considered a binary string of m elements, and we append a prefix equal to 1. This only because the string
          can be empty;
          \item Choose a prime number p in the universe U, and assume that $U = 2^{64}$, so we want that the prime it is big but 
          not much big because we want manupulate the number in a fast way.
          \item Compute the fingerprint with the following with the following formula $fingerprint += (fingerprint << 1) + bytes[i]$;
      \end{itemize}
      
      Now, we miss an analysis where there is a possibility of collision of the hash function, in other words what is the 
      probability that two different strings are equal for the hash function is defined as follows:

      \begin{center}
          $Prob(error) = \frac{size\_of(bad primes))}{size\_of(prime univers)}$
      \end{center}

      Where the bad primes can be a divisor of the fingerprint.

      In other words, the probability dependens from the size of the universe and the size of the windows\footnote{Windows is the prime choosed randomlly.}.
      So, we know that the windows << than the size of the universe. As sesult the probability of error it is very low.\\


      A C++ implementation can be found at \url{https://github.com/vincenzopalazzo/cpstl/tree/master/cpp/algoritmins/karp_rabin_fingerprint}

    \subsection{Near Duplicate detection}
        In the web, and in other place we have the place to discover the similarity between
        set of elements. An example can be the following one:
        \begin{definition}
            Lets say you and I are both subscribers to Netflix, and we’ve each watched roughly 100 movies on Netflix.
            The list of movies I’ve seen is a set, and the list of movies you’ve seen is another set. 
            To measure the similarity between these two sets, you can use the \emph{Jaccard Similarity}, which is given by 
            the intersection of the sets divided by their union. That is, count the number of movies we’ve both seen, and
            divide that by the total number of unique movies that we’ve both collectively seen.
            If we’ve each watched exactly 100 movies, and 50 of those were seen by both of us, then the intersection is 50 
            and the union is 150, so our Jaccard Similarity is $1/3$.
        \end{definition}

        What seems to be the more common application of “set similarity” is the comparison of documents. 
        One way to represent a document would be to parse it for all of its words, and represent 
        the document as the set of all unique words it contains. \\
        In practice, you’d hash the words to integer IDs, and then maintain the set of IDs present in the document.\\
        It’s important to note that we’re not actually extracting any semantic meaning of the documents here, 
        probably won’t work as well, for example, for comparing documents that cover similar concepts but are otherwise completely 
        unique. we’re simply looking at whether they contain the same words. This technique of comparing documents pr
        Instead, the applications of this technique are found where there’s some expectation that the documents will specifically 
        contain a lot of the same words.
        
        \subsubsection{Jaccard Similarity}
            We declare that page A and B are near duplicated if the intersection of SA and SB is large according 
            to an arbitrary measure, like the \emph{Jaccard similarity} defined as follows:

            \begin{center}
                $Jaccard_Similarity(S_a, S_b) = \frac{S_a \cap S_b}{S_a \cup S_b}$
            \end{center}

            This process requires a big amount of space, and the full cost of computing the intersection over the whole shingling sets.
        \subsubsection{Shingles}
        
            A small detail here is that it is more common to parse the document by taking, for example, each possible string of 
            three consecutive words from the document (e.g., “A small detail”, “small detail here”, “detail here is”, etc.) 
            and hashing these strings to integers. 
            This retains a little more of the document structure than just hashing the individual words. 
            This technique of hashing sub-strings is referred to as “shingling”, and each unique string is called a “shingle”.
            A very common shingling algorithm it is the \emph{k-shingle} algorithm, that divided the document in words of length k.

        \subsubsection{First version of Duplication Problem}

            Let introduce a problem that is related to finding all the web page that are near duplicate, and we can use
            the Jaccard Similarity over the sequences of Shingles. In this case we obtain a value $0 < jaccard\_similarity(SA, SB) < 1$
            and the following code contains the solution TODO.\\

            But what happen when the document are big and we use the Jaccard similarity over two big sets?
            Let’s say you have a large collection of documents, and you want to find all of the pairs of documents that are near-duplicates of each other. 
            You’d do this by calculating the Jaccard similarity between each pair of documents, and then selecting those with a similarity above some threshold.
            To compare each document to every other document requires a lot of comparisons! It’s not quite N-squared comparisons, since that would include doing 
            a redundant comparison of ‘a’ to ‘b’ and ‘b’ to ‘a’, as well as comparing every document to itself.

            An improvement of this solution is reported after the introduction of the Min Hashing technique \ref{sec:minhash}
        \subsubsection{Min Hashing}\footnote{Source: \url{https://mccormickml.com/2015/06/12/minhash-tutorial-with-python-code/}}
        \label{sec:minhash}
            The Min-Hash algorithm will provide us with a fast approximation to the Jaccard Similarity between two sets.
            For each set in our data, we are going to calculate a Min-Hash signature. 
            The Min-Hash signatures will all have a fixed length, independent of the size of the set.

            To approximate the Jaccard Similarity between two sets, we will take their Min-Hash signatures, and simply count the number 
            of components which are equal. If you divide this count by the signature length, you have a pretty 
            good approximation to the Jaccard Similarity between those two sets.
            We can compare two Min-Hash signatures in this way much quicker than we can calculate 
            the intersection and union between two large sets. This is partly because the Min-Hash signatures tend 
            to be much shorter than the number of shingles in the documents, and partly because the comparison operation is simpler.

            The reason Min-Hash works is that the expected value of the MinHash similarity (the number of matching components divided by the signature length) 
            can be shown to be equal to the Jaccard similarity between the sets. 
            We’ll demonstrate this with a simple example involving two small sets:\\
                \begin{itemize}
                    \item Set A: [32, 3, 22, 6, 15, 11]
                    \item Set B: [15, 30, 7, 11, 28, 3, 17]
                \end{itemize}

            In the Set A and Set B There are three items in common between the sets (highlighted in bold), and 10 unique 
            items between the two sets. Therefore, these sets have a Jaccard similarity $jaccard\_similatiry(SA, SB) = 3/10$.

            Let’s look first at the probability that, for just a single MinHash signature component, 
            we end up computing the same MinHash value for both sets.

            The MinHash calculation can be thought by the following steps:
            \begin{itemize}
                \item Taking the union of the two sets;
                \item Shuffling them in a random order\footnote{the actual value used in the MinHash signature is not the original item ID, 
                but the hashed value of that ID—but that’s not important for our discussion of the probabilities};
            \end{itemize}

            So if you take the union of these two sets Union: [32, 3, 22, 6, 15, 11, 30, 7, 28, 17], and shuffle the element inside the set
            and the similarity between the two Set it is given by $item\_in\_common / tot\_item\_of\_union$. In this case, it is $3/10$, and 
            it is equal to the Jaccard similarity of the set.

            \begin{definition}
                This introduce a lemma that the probability that two set are equal it is equal to the Jaccard Similarity.
            \end{definition}

            \subsubsection{Cosine Techniques}\footnote{Source: https://www.machinelearningplus.com/nlp/cosine-similarity/}
                
                Cosine similarity is a metric used to determine how similar the documents are irrespective of their size.
                Mathematically, it measures the cosine of the angle between two vectors projected in a multi-dimensional space. 
                In this context, the two vectors I am talking about are arrays containing the word counts of two documents.

                Cosine similarity is generally used as a metric for measuring distance when the lenght of the vectors does 
                not matter. This happens for example when working with text data represented by word counts. We could assume 
                that when a word (e.g. science) occurs more frequent in document 1 than it does in document 2, that document 1 
                is more related to the topic of science. However, it could also be the case that we are working with 
                documents of uneven lengths (Wikipedia articles for example). Then, science probably occurred more 
                in document 1 just because it was way longer than document 2.

                Cosine similarity corrects for this, Text data is the most typical example for when to use this metric. 
                However, you might also want to apply cosine similarity for other cases where some properties of the instances 
                make so that the weights might be larger without meaning anything different. Sensor values that were captured 
                in various lengths (in time) between instances could be such an example.

                We define the cosine similarity with the following formula

                \begin{center}
                    $cosine\_sim = \dfrac{
                        \sum_{\substack{ 0<i<n }} A_i B_i
                        }{
                            \sqrt{
                                \sum_{\substack{ 0<i<n }} A_i^2
                            }
                            \sqrt{
                                \sum_{\substack{ 0<i<n }} B_i^2
                            }
                        } = ||A|| \cdot ||B||$
                \end{center}

                The two element are equal with the following probability probability

                \begin{center}
                    $prob(hash(A) == hash(B)) = 1 - \dfrac{angle\_between(A, B)}{\pi}$
                \end{center}