\section{Compression of Documents}

In a modern search engine the raw documents are neededfor  various applications, one  of  the  more  common is 
thedynamic extraction of  a  snippet depending on  the  query.The most important tradeoff in data compressione 
is givenby the inverse proportionality of the compression rate andthe decompression speed. Recently many 
technologies havebeen developed, among the  others we  recall  Snappy and Brotly by Google and LZFSE by Apple.

    \subsection{LZ77}
        LZ77\footnote{Source: https://towardsdatascience.com/how-data-compression-works-exploring-lz77-3a2c2e06c097}
        it is a family of algorithm that fill under a dictionary compression tecnique. It is used
        in the famus gzip tools used by all the UNIX based machine. The basic idea under this algorithm it is 
        to find repeated substring in a document. 
        
        \begin{definition}
            Given a document it  generates a  set  of  triplets of  the  form {\bf (offset, length,  next-char)}  
            by  scanning the  documentusing a  fixed  size  buffer window.
        \end{definition}

        In particular, with the compression type of LZ77 we can divide the array in two part like the figure \ref{fig:lz77_ex}, 
        where we have the following method

        \begin{figure}[H]
            \centering
            \begin{tikzpicture}
                %\pgftransparencygroup
                \nodes{2, 4, 3, 1, 6, 7, 8, 9}
                %\endpgftransparencygroup
                %\pgftransparencygroup
                \brckt[2]{0}{4}{Searching buffer}
                %\endpgftransparencygroup
                %\pgftransparencygroup
                \brckt[3]{5}{7}{Look ahaed buffer}% used ~ to push the label
            \end{tikzpicture}
            \label{fig:lz77_ex}
        \end{figure}
    
    
        
        \begin{itemize}
            \item {\bf Search Buffer}: The segment that we have already seen so far, and when the algorithm will
                                       perform the search of one token.
            \item {\bf Look ahaed buffer}: The segment that we miss to analizy, and where the algorithm can move the 
                                         windows\footnote{The widows is the lenght of the token that we need to seach.}
        \end{itemize}

        During the compression session, the algorithm will iterate over the source that we want compress, 
        and when we can have different situations, described below:
        
        \begin{itemize}
            \item The token in the look ahaed buffer it is found in the srach buffer, and the step will generate
            the following touple (offset, lenght, nextToken). Where the component of the token are described below.
            \item the token in the look ahaed buffer it is not found. In this case the the algorithm generate a special
            token called also, null token that looks like (0, 0, nextToken). This mean that the nextToken it is unique until now.
        \end{itemize}

        The token produced by the algorithm contains the following information:
        \begin{itemize}
            \item offset: The number of "jump"/distance from the tocken that we are seaching and the first occurence found.
            \item lenght: The lenght of the token.
            \item nextToken: The next token that we have.
        \end{itemize}

        The running time of this algorithm it is slow for a big input where make the compression, in this case the size of the
        seaching buffer it is fixed, in this way we can acheive a better perfomance.
    
    \subsection{Compression and Networking}

        These days we transfer a lot of data across the network, and this can be a very slow operation, also
        because we send redundand data. This is problem that need to be resolved, and the compression can help
        us.

        To solve this problem there are two standard techinque, that are:
        
        \begin{itemize}
            \item {\bf Caching}: This tecnique avoid to send the same items multiple times;
            \item {\bf Compression}: This technique remove redundancy during the trasmission.
        \end{itemize}

        \subsubsection{Main Approach to Data Compressione}
            When we considerer to apply data compression, we need to take into count two main cases:

            \begin{itemize}
                \item Common knowledge between sender, and receiver: The sender send a file $f\prime$ encoded from a file $f$, and
                      this file it is send throw the receiver.
                      \begin{itemize}
                        \item Tipically the common approach used is called delta compression.
                      \end{itemize}
                \item Partial knowlege shared bytween sender and receiver, in this case the received have some part of the file,
                      or a old version of it, and in this case the sender share with the receiver only the newest part.
                      \begin{itemize}
                        \item Update unstrucutred file, like a source coude file, or zip file.
                        \item Update record based data, \emph{set reconsiliation} that update a set lits, like an agenda.
                      \end{itemize}
            \end{itemize}

            In terms of implementation, 

            \begin{itemize}
                \item {\bf Delta Compression}: Implemented by tool like \emph{diff}, \emph{zdelta}, \emph{REBL}, where these
                algorithm are used to compress a file $f$ in a new file $f'$. As result we obtain a speed-up the web access by sending the from the
                requested file. In this case a new file is sent.
                \item {\bf File Synchronizzation}: Implemented by tool like \emph{rsynch}, \emph{zsync}, where these algorithm
                help to update a old file on the cliend with the new one that exist only on the server.
            \end{itemize}

            \subsubsection{Z-delta compression}

                We will take in consideration the z-delta compression from two parts (one to one), and we define the following problem.

                \begin{definition}
                    We have file $f_{known}$ that is known by the twoo parts, and we have a file $f_{new}$ that is known only from
                    one pars. The goal it is to compute a file with the differences $f_d = f_{new} - f_{known}$ where $f_d$ is of 
                    the minimum size to derive $f_{new}$ from $f_{known}$.
                \end{definition}

                The solution of at this problem with the z-delta algorithm use the lz77 schema where we will go to compute a touple
                of elements.

            \subsubsection{How Z-delta works}
                We see that the algorithm that z-delta use under the hood it is lz77 in some way to obtains the difference of the file
                to sent.

                In particular the algorithm takes the $f_{known}$ and concatenate the $f_{new}$. After that on the concatenation
                it will apply lz77 algorithm, where $f_known$ it is the search buffer. So, it will search if some part of $f_{new}$
                it is already known, and if it is compress with lz77 rule. In conclusion, the result of lz77 is the content of 
                the $f_d$.

            \subsection{File Syncronizzation}

                In this type of problem, there is a client and a server, where the server has a new version of a file, and it want update
                the content store on the client side. The most famus approach to make this operation are:
                
                \begin{itemize}
                    \item {\bf rsync}: With this algorithm the client start to comunicate with the server, where the 
                    client it is very poor in terms of power.
                    \item {\bf zsync}: This algorithm use the opposite one approach, so it start to communicate from the server.
                \end{itemize}

            \subsubsection{rsync}
                How mension before this type of algorithm start from the client, where the client can be very poor in
                terms of power.

                The process follow the following steps:
                \begin{itemize}
                    \item Split the file in block of fixed size;
                    \item Hasing each block;
                    \item Send all the hash to the server;
                    \item The server from the knowlge that learn from the client hashes, send back the new file part;
                \end{itemize}

                Note that the server know only the information of the size of the block, and not the meaning of the hash,
                this mean that serve need to shift the window by on char at the time. In this case, the following case can
                occur:
                \begin{itemize}
                    \item The element it is not found in the block;
                    \begin{itemize}
                        \item Shift the window by one step, and the single character in out of the windows it is send
                        to the client.
                    \end{itemize}
                    \item A element it is found with the same hash, in this case the server send back to the client the hash.
                \end{itemize}

                However, rsync has the following problem:

                \begin{itemize}
                    \item What it the best choice of the block size where to split the file? It is a problematic choice.
                    \item The position of the changes in the file can be not destroy the looup in the server.
                    \item Hight loading over the server, because the look up in the sliding windows can be costly.
                \end{itemize}

                \subsubsection{zsync}

                    In confront of the rsync, this algorithm want to reduce the loading on the server, and the reason it because
                    the client nowadays are very powerful, and we want distribute the load across client and server.

                    The algorithm, is based on the following steps:

                    \begin{itemize}
                        \item Start from the server, where split the file in blocks of fixed side, and 
                             and hashing it.
                        \item Send the hash computed on the client, to the client;
                        \item The client make a lookup of the hashes received by the server, and try to find the 
                             the hashes that are in commons.
                        \item The client send back a sequence of bits where:
                        \begin{itemize}
                            \item value is 0: When there is a change of the server file;
                            \item value it 1: where there is a match.
                        \end{itemize}
                        \item The server create a new file $f\prime$ with the the block that receive the 1 from the 
                        client are punt in the search buffer (this in terms of lz77), and the block that receive the 0s
                        are punt in the look ahaed part.
                        \item On this file apply the lz77 algorithm, send this file to the client.
                        \item The client sort the block with the 1 and 0 like the server, because it know this information
                        and apply the decompression of the lz77 algorithm.
                    \end{itemize}