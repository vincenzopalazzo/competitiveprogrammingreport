# Advanced Data Bases 2021/2022

## Table of Content

- Class Program
- License

## Class Program

- [ ] [Persistent memory and buffer management](https://elearning.di.unipi.it/pluginfile.php/54055/mod_resource/content/1/1.persmemoryandbuffers.pdf)
- [ ] [Sequential organizations](https://elearning.di.unipi.it/pluginfile.php/54071/mod_resource/content/2/2.seqorganizations.pdf)
- [ ] [Hash organizations](http://pages.di.unipi.it/ghelli/bd2/3.hashorganizations.pdf)
- [ ] [Tree organizations](http://pages.di.unipi.it/ghelli/bd2/4.treeorganizations.pdf)
- [ ] [Organizations for non key-attributes and multidimensional organizations](http://pages.di.unipi.it/ghelli/bd2/5.nonkey.pdf)
- [ ] [Access methods manager](http://pages.di.unipi.it/ghelli/bd2/6.accessmethods.pdf)
- [ ] [Realization of relational operators](http://pages.di.unipi.it/ghelli/bd2/7.relationaloperators.pdf)
- [ ] [Query optimization](http://pages.di.unipi.it/ghelli/bd2/8.queryoptimization.pdf)
- [ ] [Transactions](http://pages.di.unipi.it/ghelli/bd2/9.transactions.pdf)
- [ ] [Concurrency](http://pages.di.unipi.it/ghelli/bd2/10.concurrency.pdf)
- [ ] [Physical design](http://pages.di.unipi.it/ghelli/bd2/11.physicaldesign.pdf)
- [ ] [Decision support systems](http://pages.di.unipi.it/ghelli/bd2/12.DSSsystems.pdf)
- [ ] [Column databases](http://pages.di.unipi.it/ghelli/bd2/13.ColumnDatabase.pdf)
- [ ] [Parallel and distributed databases](http://pages.di.unipi.it/ghelli/bd2/14.ParallelDistributed.pdf)
- [ ] [Big Data](http://pages.di.unipi.it/ghelli/bd2/1.BigData.pdf)

## License

<div align="center">
  <img src="https://opensource.org/files/osi_keyhole_300X300_90ppi_0.png" width="150" height="150"/>
</div>

Advanced Data Bases 2021/2022 class notes.

Copyright (C) 2021 Vincenzo Palazzo <vincenzopalazzodev@gmail.com>.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
